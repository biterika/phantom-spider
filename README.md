### biterika/phantom-spider


####INSTALL:

***Ubuntu 16.04 LTS (Xenial Xerus)***

1. установить timezone
```
dpkg-reconfigure tzdata

```

2. cmd+c - cmd-v в консоль
```
apt-get update

apt-get install -y language-pack-en-base software-properties-common \
python-software-properties build-essential upstart
```

3. cmd+c - cmd-v в консоль
```
apt-get update

add-apt-repository main -y
add-apt-repository universe -y
add-apt-repository restricted -y
add-apt-repository multiverse -y
```

4. cmd+c - cmd-v в консоль
```
apt-get update

apt-get install mc htop atop iftop \
    git ntp psmisc curl unzip wget nano -y
```

5. cmd+c - cmd-v в консоль
```
apt-get install -y g++ flex bison gperf ruby perl \
  libsqlite3-dev libfontconfig1-dev libicu-dev \
  libfreetype6 libssl-dev \
 libpng-dev libjpeg-dev python libx11-dev libxext-dev
```

6. nodejs & pm2
```
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
apt-get install -y nodejs
npm install pm2 -g

```


8. сам продукт (нужно ввести пароль git-репозитария от biterika-user)
```
mkdir -p /usr/share/biterika/spider
cd /usr/share/biterika/spider
git init
git remote add origin https://biterika-user@bitbucket.org/biterika/spider.git
git fetch origin
git checkout -b master --track origin/master
git fetch --all && git reset --hard origin/master


cd /usr/share/biterika/spider/agent
npm install
pm2 start agent.js
pm2 startup
pm2 save
```

#### env-параметры

```
TASK_URL=http://task_host:8787/get_task

add_trader_host=add_trader_host:80

# var task_server_url_closed = 'http://'+add_trader_host+'/api/messages/closed';
# var task_server_url_tradelinks = 'http://'+add_trader_host+'/api/tradelinks/strings';
```

```
working_dir: /usr/share/biterika/spider/agent
run: node agent.js
debug run one instance with console log : node agent.js debug

кол-во фантомов: ps -Al | grep -c phantomjs
web-стата: http://host:8564/health
```




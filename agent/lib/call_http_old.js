const debug = require('./debug');
const time = require('./time');

const http = require('http');
const request = require('request');
const querystring = require('querystring');

var colors = require('chalk');
if (process.argv.indexOf('--no-color') >= 1) {
    debug.log('--no-color');
    colors = new colors.constructor({enabled: false});
}

var ok = colors.bgBlack.green.bold('    [ok]    ');
var bad = colors.bgBlack.red.bold('    [bad]   ');


var max_rety_count = 2;
var statusCode_get = 200;


function get(method, host, port, path, query_str, rety_count, callback) {

    if (!rety_count) rety_count = 0;
    rety_count++;
    if (rety_count > 1) {
        debug.log('retrying http call rety_count = ' + rety_count);
    }
    if (rety_count > max_rety_count) {
        debug.log('retrying limit finish, stop http call');
        return false;
    }

    debug.log('     http call start');

    var path_query = path;
    if (query_str) {
        path_query = path + '?' + querystring.stringify(query_str);
    }

    var options = {
        host:    host,
        port:    port,
        path:    path_query,
        method:  method,
        headers: {}
    };


    debug.log('     http call options '+JSON.stringify(options));
    var body_str = '';


    var req_ext = http.request(options, (res_ext) => {

        debug.log(colors.gray('http ' + rety_count + ' statusCode = ' + res_ext.statusCode));


        res_ext.on('data', function (chunk) {
            body_str += chunk;

            debug.log(`res_ext.on('data'`);
        });

        res_ext.on('end', function () {

            callback(
                null,
                {
                    body:       body_str,
                    statusCode: res_ext.statusCode,
                    headers:    res_ext.headers

                }
            );
        });


        if (res_ext.statusCode == statusCode_get) {
            debug.log(ok + 'call_http [ok]');

        } else {
            debug.log(bad + 'http_call: fail statusCode(' + res_ext.statusCode + ' ' + body_str + ') no good ...');
        }
    });

    req_ext.on('response', function () {
        debug.log(`info: req_ext.on('response'`);
    });

    req_ext.on('error', function (err) {
        console.error('http errr (' + rety_count + ') ' + JSON.stringify(err));
        callback(
            err,
            null
        );
    });
    req_ext.end();

}
exports.get =get;
// v.1.0.0

//   moment().utcOffset('+07:00').format('YYYY-MM-DD HH:mm:ss Z')
// moment().utcOffset('+07:00').format('YYYY-MM-DD HH:mm:ss Z')

var moment = require('moment');

var time_start = parseInt(new Date().getTime()/1000);

var time = function(){
    var d = new Date();
    var h = d.getHours();if (h <= 9) h = "0" + h;
    var m = d.getMinutes();if (m <= 9) m = "0" + m;
    var s = d.getSeconds();if (s <= 9) s = "0" + s;
    return h+':'+m+':'+s;
};
var time_delta = function(){
    var time_now = parseInt(new Date().getTime()/1000);
    return time_now - time_start;
};
var time_int = function(){
    return parseInt(new Date().getTime()/1000)
};

function reset(){
    time_start = parseInt(new Date().getTime()/1000);
}

function next_hour_zero_ts() {

    return parseInt(
        moment(
            moment.utc().add(1, 'hour').format('YYYY-MM-DD HH:00:00')
        ).format('X')
    );
    //console.log(utc_hour, moment(utc_hour,'X').format('YYYY-MM-DD HH:mm:ss Z'));
}
exports.next_hour_zero_ts = next_hour_zero_ts;

function next_day_zero_ts() {

    return parseInt(
        moment(
            moment.utc().add(1, 'day').format('YYYY-MM-DD 00:00:00')
        ).format('X')
    );
    //console.log(utc_hour, moment(utc_hour,'X').format('YYYY-MM-DD HH:mm:ss Z'));
}
exports.next_day_zero_ts = next_day_zero_ts;

function utc_hour_zero_dateTime () {

    return moment.utc().format('YYYY-MM-DD HH:00:00');
    //console.log(utc_hour, moment(utc_hour,'X').format('YYYY-MM-DD HH:mm:ss Z'));
}
exports.utc_hour_zero_dateTime = utc_hour_zero_dateTime;


function utc_dateTime () {
    return moment.utc().format('YYYY-MM-DD HH:mm:ss');
}

function dateTimeZ() {
    return moment().utcOffset('+07:00').format('YYYY-MM-DD HH:mm:ss Z');
}
exports.dateTimeZ = dateTimeZ;


function time_progress_prc(interval,ts,opt) {

    if (interval = 'hour') {

        var hour_left_second = ts - time_int();
        var hour_left_minutes = Math.round(hour_left_second/60);
        var hour_left_percent = Math.round(((hour_left_second) / (60 * 60)) * 100);
        var hour_progress_percent = 100 - hour_left_percent;
        //console.log('hour_s_left_percent', hour_left_percent, hour_progress_percent);
        if (opt && opt=='hour_left_minutes') {
            return hour_left_minutes;
        } else {

            return hour_progress_percent;
        }

    }
    else if (interval = 'day') {

        var day_left_percent = Math.round(((ts - time_int()) / (60 * 60 * 24)) * 100);
        var day_progress_percent = 100 - day_left_percent;
        //console.log('day_s_left_percent', day_left_percent,day_progress_percent);

        return day_progress_percent;

    }
}
exports.time_progress_prc =time_progress_prc;

exports.utc_dateTime =utc_dateTime;

exports.reset = reset;
exports.time = time;
exports.time_delta = time_delta;
exports.time_int = time_int;


var questions = [
    {q: `What's closer to Earth: Moon or Sun?`, a: `Moon`},
    {q: `Calculate: 3+2*2`, a: `7`},
    {q: `Calculate: 43 Ñ… 2`, a: `86`},
    {q: `What is farther from Sun: Earth, Saturn or Mars?`, a: `Saturn`},
    {q: `Calculate: 3 plus 3*2`, a: `9`},
    {q: ``, a: ``},
    {q: ``, a: ``},
];

function search(quest) {
    if (!quest) return false;

    var questionText = quest.replace('Please, answer a Question:\n','');

    var answer = null;
    for (let i in questions) {
        if (questions.hasOwnProperty(i)) {
            if (questions[i].q == questionText) {
                answer = questions[i].a;
                break;
            }
        }
    }
    console.log(answer);

    return answer;
}
exports.search = search;



//const debug = require('./debug');
//const time = require('./time');

const http        = require('http');
const request     = require('request');
const querystring = require('querystring');
const urld        = require('url');

var ok  = '    [ok]    ';
var bad = '    [bad]   ';


var max_rety_count = 2;
var statusCode_get = 200;

var log = function () {
};

if (process.env.hasOwnProperty('debug_log') && process.env.debug_log) {
    log = console.log.bind(console, 'call_http.js');
    console.log('call_http.js debug_log enabled')
}

/*var a = {
    method: 'POST',
    url: '',
    data: {},
    ans_format: 'json'
};*/

function send(args, callback, rety_count) {

    var method        = args.method || 'GET';
    var url           = args.url;
    var query     = args.query || null;
    var post_data_obj = args.data || null;
    var headers       = args.headers || {};
    var ans_format    = args.ans_format || null;


    if (!rety_count) rety_count = 0;
    rety_count++;
    if (rety_count > 1) {
        log('retrying http call rety_count = ' + rety_count);
    }
    if (rety_count > max_rety_count) {
        log('retrying limit finish, stop http call');
        callback('retrying limit finish, stop http call', null);
        return false;
    }

    if (query) {
        url = url + '?' + querystring.stringify(query);
    }

    var options     = urld.parse(url);
    options.method  = method;
    options.headers = headers || {};

    var post_data = null;
    if (post_data_obj) {
        post_data = JSON.stringify(post_data_obj);
    }


    log('     http call options ' + JSON.stringify(options), ' url = ' + url);
    var body_str = '';


    var req_ext = http.request(options, (res_ext) => {

        log('http ' + rety_count + ' statusCode = ' + res_ext.statusCode);



        res_ext.on('data', function (chunk) {
            body_str += chunk.toString();
            //log('chunk ',chunk.toString());

        });

        res_ext.on('end', function () {
            if (res_ext.statusCode == statusCode_get) {
                log(ok + 'call_http [ok]');

                if (ans_format == 'json') {
                    var res_obj = null;
                    var error   = false;
                    try {
                        res_obj = JSON.parse(body_str)
                    } catch (err) {
                        error = true;
                        log('http_call: error JSON parse ' + body_str + ') no good ...', err.message);
                        if (callback) {
                            callback(
                                'http_call: error JSON parse ' + body_str + ') no good ...', err.message,
                                null
                            );
                            callback = null;
                        }


                    }

                    if (!error) {
                        if (callback) {
                            callback(
                                null,
                                res_obj
                            );
                            callback = null;
                        }
                    }


                }
                else {
                    callback(
                        null,
                        {
                            body:       body_str,
                            statusCode: res_ext.statusCode,
                            headers:    res_ext.headers

                        }
                    );
                }

            } else {
                log(bad + 'http_call: fail statusCode(' + res_ext.statusCode + ' ' + body_str + ') no good ...');
                callback(
                    'http_call: fail statusCode(' + res_ext.statusCode + ' ' + body_str + ') no good ...',
                    null
                );
            }

        });


    });

    req_ext.on('error', function (err) {
        console.error('http errr (' + rety_count + ') ' + JSON.stringify(err));
        callback(
            err,
            null
        );
    });

    if (post_data) {
        req_ext.write(post_data);
    }

    req_ext.end();

}
exports.get  = send;
exports.send = send;
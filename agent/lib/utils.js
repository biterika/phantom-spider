// v.1.0.0

'use strict';
var crypto = require('crypto');
var colors = require('chalk');
var debug = require('./debug');
if (process.argv.indexOf('--no-color') >= 1) {
    debug.log('--no-color');
    colors = new colors.constructor({enabled: false});
}

function parseIPtoIPv4(ip_req) {
    var indexOfColon = ip_req.lastIndexOf(':');
    var IP = ip_req.substring(indexOfColon + 1, ip_req.length);
    return IP;
}

function parseUrlPath(urlPath) {

    //var urlPath = '/one/two/tgri';
    var pathArray = urlPath.split('/');
    pathArray.splice(0, 1);
    //console.log(pathArray)
    return pathArray;
}

function jsonParse(json) {
    var json_obj;
    try {
        json_obj = JSON.parse(json);
    } catch (err) {
        console.error('[ERROR] parse json');
    }

    return json_obj;
}

function print(title, array, fields, option) {

    var result = colors.magenta.bgYellow.bold(title) + ':\n';
    var comma = '';
    var str = '', str_part = '', str_test = '';

    for (var a in array) {
        if (array.hasOwnProperty(a)) {
            comma = '';
            str = '\n\t\t' + colors.bold(a) + ': ';
            for (var f in fields) {
                if (fields.hasOwnProperty(f)) {
                    if (array[a].hasOwnProperty(fields[f])) {

                        str_part = '' + comma + colors.bold(fields[f]) + ': ' + echo(array[a][fields[f]]);
                        str_test = str + str_part;
                        if (str_test.length > 180) {
                            result += str + comma + '\n' + '\t\t\t' + fields[f] + ': ' + echo(array[a][fields[f]]);
                            str = '';
                        } else {
                            str += str_part;
                        }
                        comma = ',\t';

                    }
                }
            }
            if (str) {
                result += str + '\n';
            }

        }
    }

    function echo(some) {
        var result = '';
        var comma2 = '';
        if (typeof some == 'string' || typeof some == 'number') {
            result += '\'' + some + '\'';
        } else if (typeof some == 'boolean') {
            if (some === true) {
                result += '\'' + colors.bgGreen(some) + '\'';
            } else {
                result += '\'' + colors.bgRed(some) + '\'';
            }
        }
        else if (typeof some == 'object' && some instanceof Array) {
            result += '[';
            for (var o in some) {
                if (typeof some[o] == 'string' || typeof some[o] == 'number' || typeof some[o] == 'boolean') {
                    result += comma2 + '\'' + some[o] + '\'';
                } else {
                    result += comma2 + '[' + typeof some[o] + ', ' + Object.prototype.toString.call(some[o]) + ']';
                }
                comma2 = ',\t';
            }
            result += ']';

        }
        else {

            result += '[' + typeof some + ', ' + Object.prototype.toString.call(some) + ']';
        }

        return result;
    }

    return result;

}


function update_elems(array, param, value, opt) {
    var result = [];
    for (var a in array) {
        if (array.hasOwnProperty(a)) {
            if (array[a].hasOwnProperty(param)) {
                if (array[a][param] == value) {
                    if (opt == 'getOne') {
                        return array[a];
                    }
                    result.push(array[a]);
                }
            }
        }
    }
    return result;
}


function get_elems(array, param, value, opt) {
    var result = [];
    for (var a in array) {
        if (array.hasOwnProperty(a)) {
            if (array[a].hasOwnProperty(param)) {
                if (array[a][param] == value) {
                    if (opt == 'getOne') {
                        return array[a];
                    }
                    result.push(array[a]);
                }
            }
        }
    }

    if (result.length > 0) {
        return result;
    } else {
        return false;
    }
}

function get_idxs(array, param, value, opt) {
    var result = [];
    for (var a in array) {
        if (array.hasOwnProperty(a)) {
            if (array[a].hasOwnProperty(param)) {
                if (array[a][param] == value) {
                    if (opt == 'getOne') {
                        return a;
                    }
                    result.push(a);
                }
            }
        }
    }

    //if (result.length > 0) {
    return result;
    //} else {
    //    return false;
    //}
}


function check_properties(obj, obj_desc, prop_list, silent) {

    var errors = [];
    if (obj) {
        for (var p in prop_list) {
            if (prop_list.hasOwnProperty(p)) {
                if (!obj.hasOwnProperty(prop_list[p])) {
                    errors.push('have not poperty "' + prop_list[p] + '"');
                }
            }
        }
    } else {
        errors.push('have not object for check properties');
    }

    if (errors.length > 0) {

        var error_str = '[ERROR] check_properties() in ' + JSON.stringify(obj_desc) + ', ';
        var comma = '';
        for (var e in errors) {
            if (errors.hasOwnProperty(e)) {
                error_str += comma + errors[e];
                comma = ' ,';
            }
        }

        if (!silent) {
            debug.error(error_str);
        }

        return false;

    } else {

        return true;
    }
}

function zerofill(num, size) {
    var s = num + '';
    while (s.length < size) s = '0' + s;
    return s;
}


var NUMBER_GROUPS = /(-?\d*\.?\d+)/g;

var mySortFn = function (a, b) {

    var aa = String(a).split(NUMBER_GROUPS),
        bb = String(b).split(NUMBER_GROUPS),
        min = Math.min(aa.length, bb.length);

    for (var i = 0; i < min; i++) {
        var x = parseFloat(aa[i]) || aa[i].toLowerCase(),
            y = parseFloat(bb[i]) || bb[i].toLowerCase();
        if (x < y) {
            //console.log(a+'>'+b+' -> -1');
            return -1;
        }
        else if (x > y) {

            //console.log(a+'>'+b+' -> 1');
            return 1;
        }
    }

    //console.log(a+'>'+b+' -> 0');
    return 0;
};

function isNumber(o) {
    return !isNaN(o - 0) && o !== null && o !== "" && o !== false;
}

function hhmmss(secs) {
    var minutes = Math.floor(secs / 60);
    secs = secs % 60;
    var hours = Math.floor(minutes / 60)
    minutes = minutes % 60;
    return pad(hours) + ":" + pad(minutes) + ":" + pad(secs);
    function pad(num) {
        return ("0" + num).slice(-2);
    }
}

function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}


function getSortMethod() {
    var _args = Array.prototype.slice.call(arguments);
    return function (a, b) {
        for (var x in _args) {
            var ax = a[_args[x].substring(1)];
            var bx = b[_args[x].substring(1)];
            var cx;

            ax = typeof ax == "string" ? ax.toLowerCase() : ax / 1;
            bx = typeof bx == "string" ? bx.toLowerCase() : bx / 1;

            if (_args[x].substring(0, 1) == "-") {
                cx = ax;
                ax = bx;
                bx = cx;
            }
            if (ax != bx) {
                return ax < bx ? -1 : 1;
            }
        }
    }
}

function bytesToSize(bytes) {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}
exports.bytesToSize = bytesToSize;


function getHostName (url) {
    if (url) {

        var match = url.match(/^(https?)\:\/\/(www[0-9]?\.)?(.[^://?]+)/);
        // console.log(match);
        if (match != null && match.length > 2 && typeof match[3] === 'string' && match[3].length > 0) {

            if (match[1] == 'http' || match[1] == 'https') return match[3].toLowerCase();

        }
    }
    return false;
}


function checksum(str, algorithm, encoding) {
    return crypto
        .createHash(algorithm || 'md5')
        .update(str, 'utf8')
        .digest(encoding || 'hex')
}


function isArrayList(somevar){
    if( Object.prototype.toString.call( somevar ) === '[object Array]' ) {
        if (somevar.hasOwnProperty(0)) {
            if (typeof somevar[0] === 'string') {
                return true;
            }
        }
    }

    return false;
}
exports.isArrayList = isArrayList;

function isArray(somevar){
    if( Object.prototype.toString.call( somevar ) === '[object Array]' ) {
        return true;
    }
    return false;
}
exports.isArray = isArray;

function mem_use(){
    var mem_use = process.memoryUsage();
    var mem_use_h = {
        rss: bytesToSize(mem_use.rss),
        heapTotal: bytesToSize(mem_use.heapTotal),
        heapUsed: bytesToSize(mem_use.heapUsed),
    };
    return mem_use_h;
}
exports.mem_use = mem_use;




exports.checksum = checksum;

exports.getHostName = getHostName;

exports.parseIPtoIPv4 = parseIPtoIPv4;
exports.parseUrlPath = parseUrlPath;
exports.jsonParse = jsonParse;
exports.print = print;

exports.get_elems = get_elems;
exports.get_idxs = get_idxs;

exports.check_properties = check_properties;
exports.zerofill = zerofill;
exports.mySortFn = mySortFn;
exports.isNumber = isNumber;
exports.hhmmss = hhmmss;
exports.cleanArray = cleanArray;

// console.log(isArrayList([]));
// v.1.1.0
'use strict';

var LOG_PORT = 9090;


var config = require('./config').obj;

if (process.argv.indexOf('debug') !== -1) {
    console.log('agrv debug detected');
    config.console_info=true;
    config.console_info_subtype.wc=true;
    config.console_log = true;
    config.console_error = true;
}

if ((process.env && process.env.hasOwnProperty('showlog')) || process.argv.indexOf('--show-log') !== -1) {
    console.log('detecting env showlog - logger enable');
    config.console_log = true;
    config.console_error = true;
    config.console_info = true;
    config.elastic_trace_log= true;
}
if ((process.env && process.env.hasOwnProperty('nolog'))  || process.argv.indexOf('--no-log') !== -1) {
    console.log('detecting env nolog - log to console disable');
    config.console_log = false;
    config.console_error = true;
    config.console_info = false;
    config.elastic_trace_log= false;
}



var time = require('./time');
var colors = require('chalk');
if (process.argv.indexOf('--no-color') !== -1) {
    //console.log('--no-color');
    colors = new colors.constructor({enabled: false});
}
var fs = require('fs');
//const Console = require('console').Console;
var node_name = '';
var write_to_file = false;
var os = require('os');

/*
 const output = fs.createWriteStream('./log.log');
 const errorOutput = fs.createWriteStream('./log.log');
 const logger = new Console(output, errorOutput);
 */

function log() {
    try {
        if (config.console_log) {
            [].unshift.call(arguments, colors.bgYellow.blue.bold('[' + JSON.stringify(os.loadavg()) + ']'));
            [].unshift.call(arguments, colors.red(time.time()));
            console.log.apply(console, arguments);

        }
        logger.apply(logger, arguments);
    } catch (err) {
        if (config.console_error) {
            console.error(err.stack);
        }
    }
}
function info() {
    try {
        if (config.console_info) {

            if (arguments.hasOwnProperty(0) || arguments.hasOwnProperty(1)) { // tds_schems
                if (config.console_info_subtype.hasOwnProperty(arguments[0]) || config.console_info_subtype.hasOwnProperty(arguments[1])) {
                    if (config.console_info_subtype[arguments[0]] || config.console_info_subtype[arguments[1]]) {
                        [].unshift.call(arguments, colors.bgYellow.blue.bold('[' + node_name + ']'));
                        [].unshift.call(arguments, colors.red(time.time()));
                        console.log.apply(console, arguments);
                    }
                }
            }

        }
        logger.apply(logger, arguments);
    } catch (err) {
        if (config.console_error) {
            console.error(err.stack);
        }
    }
}
function error() {
    try {
        if (config.console_error) {
            [].unshift.call(arguments, colors.bgWhite.red.bold('[ERROR]'));
            [].unshift.call(arguments, colors.bgWhite.red.bold('[' + node_name + ']'));
            [].unshift.call(arguments, colors.bgWhite.red.bold(time.time()));
            console.error.apply(console, arguments);
        }
        logger.apply(logger, arguments);
    } catch (err) {
        if (config.console_error) {
            console.error(err.stack);
        }
    }
}

exports.log = log;
exports.info = info;
exports.error = error;


//   //////////////////////////////


/* web-interface */
var express = require('express');
var app = express();
var http_web = require('http').Server(app);
var io = require('socket.io')(http_web);


var moment = require('moment');
const HOSTNAME = os.hostname();
//console.log('my HOSTNAME: ' + HOSTNAME);

app.set('view engine', 'ejs');


app.get('/checkHealth', function (req, res) {
    res.removeHeader('X-Powered-By');
   // console.log('... /checkHealth');
    res.end('checkHealth okay');
});

app.get('*', function (req, res) {
    res.removeHeader('X-Powered-By');

    var page = '/log';
    var pages = {
        '/log': {
            view:    'log',
            headers: ['jquery', 'bootstrap', 'socketio'],
            data:    {},
        }
    };

    res.render('layout/log', {
        view:     pages[page].view,
        headers:  pages[page].headers,
        data:     pages[page].data,
        //me_node:  node_list.get_me_node(),
        //req_url:  req_url,
        web_host: HOSTNAME + '.rancher.internal'
    });
});


////////     functions     ////////

function logger() {
    var arguments_arr = [];
    for (var i = 0; i < arguments.length; arguments_arr[i] = arguments[i], i++) {
    }
    io.emit('chat message', time.time() + ' ' + JSON.stringify(arguments_arr));
}

io.on('connection', function (socket) {
    log(colors.bgRed('--> a user connected'));
    logger('--> new user connected here, say hello to him ;) ')

    //io.emit('chat message', 'hyiii');
    //socket.broadcast.emit('hi');
    var slimer_amount=0;
    socket.on('chat message', function (msg) {
        log('chat message: ' + msg);
        io.emit('chat message', 'chat message: ' + msg);

        // команды
/*        if (msg == 'run') {
            slimer_amount = run_slimer();
            io.emit('chat message', 'ok - iam starting slimer '+slimer_amount);
        } else if (msg == 'monitor') {
            monitor();
            io.emit('chat message', 'ok - monitor run');
        } else if (msg == 'stop') {
            stop_slimer();
        }else  {
            var result = msg.match(/^run ([0-9]{1,3})$/);
            if (result) {
                if (result[1]) {
                    if (result[1]>0) {
                        run_slimers_amount(result[1]);
                        io.emit('chat message', '$$$ START RUN SLIMER - AMOUNT : ' + result[1]);
                    }
                }
            }
        }*/



    });

    socket.on('disconnect', function () {
        log(colors.bgRed('<-- user disconnected'));
        logger('--> some one user disconnected ')
    });


});

function init() {
    http_web.listen(LOG_PORT, function () {
        console.log('log listening on port:' + LOG_PORT);
    });
}
exports.init=init;




exports.task = {
    lang: 'en-US',
    platform: 'Win32',
    ua: '',
    click_ok: 0,
    disable_img_js: false,

    success_message: null,
    results: {
        TRADE_FORM_SEND: {
            have:null,
            array: {
                form_url: null,
                form_url_after_submit: null,
                textPage_after_submit: null,
                success_message: null,
                result_success_message: null
            }
        }
    },
    add_status: null,
    closed_list: [],
    closed_list_match: false,
};

exports.write_task_log = false;
exports.step = 0;
exports.page_id = 0;
exports.results = {};
exports.pages_for_close =[];
exports.max_open_pages = 5;
exports.uniq_history_urls = {
    //count_all: 0,
    count_uniq: 0,
    count_with_trade_link: 0,
    time_consume: '',
    urls: [],
    urls_with_trade_link: [],
    time_consume_sec: '',
    ts_start: null
};
exports.urls_trade = [];
exports.exit_case = null;
exports.history = {urls: [], domains:[]};
exports.tradelinks = { hrefs:[], texts: []};

var form_values =[
    {
        "selector": "#fields\\[tni\\]",
        "value": "Test"
    },
    {
        "selector": "#security_answer",
        "value": "captcha"
    }
];

var form_elem, input_elems, input_elem, textarea_elems;
var trade_form_data = [];
var tmp_form_fields_data =[];
var tmp_form_textarea_data =[];
var tmp_fields_count = 0;
var form_elems = document.querySelectorAll('form');
for (var i = 0; i < form_elems.length; i++) {

    tmp_form_fields_data =[];
    tmp_form_textarea_data =[];
    tmp_fields_count = 0;

    input_elems = form_elems[i].querySelectorAll('input');

    for (var i2 = 0; i2 < input_elems.length; i2++) {
        if (input_elems[i2].type!=='hidden' && input_elems[i2].type!=='submit') {
            tmp_fields_count++;
            tmp_form_fields_data.push({
                tag_name: 'input',
                tag_index: i2,
                name: input_elems[i2].name,
                type: input_elems[i2].type,
                placeholder: input_elems[i2].placeholder,
                id: input_elems[i2].id,
                class: input_elems[i2].className
            });
        }
    }

    textarea_elems = form_elems[i].querySelectorAll('textarea');

    for (var i3 = 0; i3 < textarea_elems.length; i3++) {
        tmp_fields_count++;
        tmp_form_fields_data.push({
            tag_name: 'textarea',
            tag_index: i3,
            name: textarea_elems[i3].name,
            type: textarea_elems[i3].type,
            placeholder: textarea_elems[i3].placeholder,
            id: textarea_elems[i3].id,
            class: textarea_elems[i3].className
        });
    }

    trade_form_data.push({
        form: {
            tag_name: 'form',
            tag_index: i,
            fields_amount: tmp_fields_count,
            name: input_elems[i].name,
            method: form_elems[i].method,
            id: form_elems[i].id,
            class: form_elems[i].className
        },
        fields: tmp_form_fields_data,
    });
}

console.log('trade_form_data = ',JSON.stringify(trade_form_data,'','\t'));

var max_fields_amounts=0;
var form_idx_with_max_fields_amount;
for (form_idx in trade_form_data) {
    console.log(form_idx,trade_form_data[form_idx].form.fields_amount);
    if (trade_form_data[form_idx].form.fields_amount > max_fields_amounts) {

        console.log('max form_idx',form_idx);

        max_fields_amounts = trade_form_data[form_idx].form.fields_amount;
        form_idx_with_max_fields_amount = form_idx;
    }
}

console.log('0 start');

//return {form_data: trade_form_data[form_idx_with_max_fields_amount].fields,verbose_form_data: trade_form_data};
if (form_idx_with_max_fields_amount) {
    console.log(1)
    var main_form_el= form_elems[form_idx_with_max_fields_amount];

    console.log('main_form_el = ',main_form_el);

    if (false) {
        main_form_el.submit();
    }
    else {
        console.log('2 start fill');

        var field_selector, field_value, field_el;
        for (fv_idx in form_values) {

            field_selector = form_values[fv_idx].selector;
            field_value = form_values[fv_idx].value;
            console.log('selector = '+form_values[fv_idx].selector);
            console.log('value = '+form_values[fv_idx].value);

            if (field_selector) {
                console.log('field_selector have');
                field_el = main_form_el.querySelectorAll(field_selector)[0];

                if (field_el) {
                    console.log('field_el ok, field_el.type = '+field_el.type);

                    if (field_value == 'captcha') {
                        var cap_area_el = field_el.parentElement;
                        console.log('cap_area_el '+cap_area_el);
                    }

                    if (field_el.type == 'checkbox') {
                        if (
                            (field_value == 'true' && !field_el.checked) ||
                            (field_value == 'false' && field_el.checked)
                        ) {
                            var ev = document.createEvent("MouseEvent");
                            field_el.setAttribute('target', '_self');
                            ev.initMouseEvent(
                                "click",
                                true /* bubble */, true /* cancelable */,
                                window, null,
                                0, 0, 0, 0, /* coordinates */
                                false, false, false, false, /* modifier keys */
                                0 /*left*/, null
                            );
                            field_el.dispatchEvent(ev);
                        }

                    }
                    else {
                        console.log('fill value = '+field_value);
                        field_el.value = field_value;
                    }
                }
                else {
                    console.log('field selecting error');
                }
            }
            else {
                console.log('field selector incorrect');
            }
        }
        // console.log(form_idx_with_max_fields_amount);

    }
}
console.log('return true;');
//return true;
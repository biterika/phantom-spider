var domain  = require('./lib/domain');
var debug   = require('./lib/debug');
var random  = require('./lib/random');
var uagent  = require('./inc/uagent');
var screens = require('./inc/screen');
var param   = require('./inc/param');
var config  = require('./inc/config');
var Q       = require('./modules/q/q.js');
var colors  = require('./modules/colors/safe.js');
//var colors  = require('./../modules/colors/lib/colors.js');


phantom.onError = function (msg, trace) {

    if (msg) console.log('phantom.onError, msg: ' + JSON.stringify(msg));
    if (trace) console.log('phantom.onError, trace: ' + JSON.stringify(trace));
    //var msgStack = ['PHANTOM ERROR: ' + msg];
    //if (trace && trace.length) {
    //    msgStack.push('TRACE:');
    //    trace.forEach(function (t) {
    //        msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function + ')' : ''));
    //    });
    //}
    //debug.error(msgStack.join('\n'));
};


var useragent = uagent.list[random.int(0, uagent.list.length - 1)];
var screen    = screens.list[random.int(0, screens.list.length - 1)].split('x');
param.task.sx = screen[0];
param.task.sy = screen[1];
param.ua_save = useragent;

var timers = {};

var url_actions = {};

var script_name = 'events.js';

debug.log(script_name, 'screen', screen, 'useragent', useragent);

exports.exec = function (obj) {

    param.step++;

    debug.log('');
    debug.log(script_name, 'Apply events...');

    var deferred = Q.defer();

    debug.log(script_name, ' obj id', obj.id, obj.url);
    debug.log(script_name, ' param', JSON.stringify(param));
    onPageCreated(obj);

    var result = {
        status: 'success'
    };
    deferred.resolve(result);

    return deferred.promise;

};

function onPageCreated(obj) {

    debug.log(script_name, ' onPageCreated() start id,url', obj.id, obj.url);


    if (param.close_popup) {
        debug.log(script_name, ' onPageCreated() ', 'Event:', 'ClosePopup', obj.id, obj.url);
        obj.close();
    }
    else {

        param.page_id++;
        debug.log(script_name, 'events  ', colors.bgBlue.white(' onPageCreated() ', 'New page Created, page_id:', param.page_id, ' url: ', obj.url));

        param.popupLoading = true;
        param.page_created = true;

        obj.domain                   = domain.getDomain(obj.url);
        obj.id                       = param.page_id;
        obj.requests                 = 0;
        obj.changed                  = 0;
        obj.settings.resourceTimeout = 10000; // 10 seconds
        obj.onResourceTimeout        = function (e) {
            console.log('events, onResourceTimeout');   // it'll probably be 408
            console.log(e.errorCode);   // it'll probably be 408
            console.log(e.errorString); // it'll probably be 'Network timeout on resource'
            console.log(e.url);         // the url whose request timed out
        };

        if (param.task.disable_img_js) {
            //obj.settings.javascriptEnabled             = false;
           // obj.settings.loadImages = false;
        }

        obj.viewportSize = {
            width:  param.task.sx || 1366,
            height: param.task.sy || 768
        };

        obj.settings.userAgent          = param.ua_save;
        obj.settings.webSecurityEnabled = false;


        // костыли
        obj.customHeaders = {'User-Agent': param.ua_save};

        obj.evaluate(function (sx, sy, lang, platform, uagent) {
            window.screen = {
                width:      sx,
                height:     sy,
                colorDepth: 24
            };
            window.navigator.__defineGetter__('javaEnabled', function () {
                return function () {
                    return true;
                };
            });
            var newNavigator           = Object.create(window.navigator);
            newNavigator._userLanguage = lang;
            newNavigator.language      = lang;
            newNavigator.platform      = platform;
            newNavigator.userAgent     = uagent;
            window.navigator           = newNavigator;
        }, param.task.sx || 1366, param.task.sy || 768, param.task.lang || 'en-US', param.task.platform || 'Win32', param.ua_save || param.task.ua);

        //debug.log(script_name, 'Set JS variables (onPageCreated) ',param.task.sx||1366, param.task.sy||768, param.task.lang||'en-US',platform||'Win32',param.ua_save||'');
        obj.onInitialized = function () {

            obj.onError = function (msg, trace) {
                if (msg) console.log('objpage phantom.onError, msg: ' + JSON.stringify(msg));
                if (trace) console.log('objpage phantom.onError, trace: ' + JSON.stringify(trace));
                //var msgStack = ['ERROR: ' + msg];
                //if (trace && trace.length) {
                //    msgStack.push('TRACE:');
                //    trace.forEach(function (t) {
                //        msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
                //    });
                //}
                //debug.error(msgStack.join('\n'));
            };


            obj.evaluate(function (sx, sy, lang, platform, uagent) {
                window.screen = {
                    width:      sx,
                    height:     sy,
                    colorDepth: 24
                };
                window.navigator.__defineGetter__('javaEnabled', function () {
                    return function () {
                        return true;
                    };
                });
                var newNavigator           = Object.create(window.navigator);
                newNavigator._userLanguage = lang;
                newNavigator.language      = lang;
                newNavigator.platform      = platform;
                newNavigator.userAgent     = uagent;
                window.navigator           = newNavigator;
            }, param.task.sx || 1366, param.task.sy || 768, param.task.lang || 'en-US', param.task.platform || 'Win32', param.ua_save || param.task.ua);
            //debug.log(script_name, 'Set JS variables (onInitialized) ',param.task.sx||1366, param.task.sy||768, param.task.lang||'en-US',platform||'Win32',param.ua_save||'');
        };

        timers[obj.id] = setTimeout(function () {
            if (typeof obj != 'undefined' && obj) {
                try {
                    obj.stop();
                } catch (e) {
                }
            }
        }, config.timeout.loading);

        /*obj.onError             = function (msg, trace) {
            var msgStack = ['ERROR: ' + msg];
            if (trace && trace.length) {
                msgStack.push('TRACE:');
                trace.forEach(function (t) {
                    msgStack.push(' -> ' + t.file + ': ' + t.line + (t.function ? ' (in function "' + t.function + '")' : ''));
                });
            }
            console.error(msgStack.join('\n'));
        };*/
        obj.onPageCreated       = function (obj) {
            onPageCreated(obj);
        };
        obj.onResourceRequested = function (requestData, networkRequest) {
            onResourceRequested(obj, requestData, networkRequest);
        };
        obj.onResourceReceived  = function (response) {
            onResourceReceived(obj, response);
        };
        obj.onUrlChanged        = function (targetUrl) {
            onUrlChanged(obj, targetUrl);
        };
        obj.onClosing           = function (closingPage) {
            onClosing(closingPage);
        };
        obj.onLoadFinished      = function (status) {
            onLoadFinished(obj, status);
        };
        obj.onConsoleMessage    = function (msg, lineNum, sourceId) {
            onConsoleMessage(obj, msg, lineNum, sourceId)
        };
    }
}


function onResourceRequested(obj, requestData, networkRequest) {
    //debug.log(script_name, 'onResourceRequested start');

    if (!param.task.task_type && requestData.url.indexOf('/redirect_task.html?') >= 0) {

        param.args = qsParseArray(requestData.url);

        debug.log(' from task_url l -> param.args = ', JSON.stringify(param.args), 'requestData.url = ', JSON.stringify(requestData.url));


        /*param.task.task_type = getQueryVariable(requestData.url,'action');
         if(!param.task.task_type || param.task.task_type == ''){
         param.task.task_type = getQueryVariable(requestData.url,'task');
         }*/

        //param.task.ip = getQueryVariable(requestData.url,'ip');
        //param.task.fake = getQueryVariable(requestData.url,'fake');
        //param.task.token_cache = getQueryVariable(requestData.url,'token_cache');

        param.task.task_type  = getQueryVariable(requestData.url, 'action');
        param.task.site       = domain.getDomain(getQueryVariable(requestData.url, 'site'));
        param.task.trader     = domain.getDomain(getQueryVariable(requestData.url, 'trader'));
        param.task.param      = domain.getDomain(getQueryVariable(requestData.url, 'param'));
        param.task.lang       = domain.getDomain(getQueryVariable(requestData.url, 'lang').split(',')[0]);
        param.task.useragent  = domain.getDomain(getQueryVariable(requestData.url, 'useragent'));
        param.task.click_task = getQueryVariable(requestData.url, 'clicks');

        useragent = param.task.useragent;
        debug.log('  ');
        debug.log(script_name, colors.red('  ################# => ', 'Set task_type:', param.task.task_type));
        debug.log('  ');
        debug.log(script_name, 'task params:', JSON.stringify(param.task));

    }

    //debug.log(script_name, 'Event:', 'Requested in page:', obj.id, requestData.url);
    //debug.log(script_name, 'freeze', obj.freeze);

    var short_url = requestData.url.substr(0, 64) + '...';

    if (requestData.url == 'data:text/html,%3Cscript%3Ewindow.close();%3C/script%3E') {
        networkRequest.abort();
        // debug.log(script_name, 'Event:', 'Requested in page: ABORT', obj.id, short_url);
    }
    else if (requestData.url.match(new RegExp('http://syndication.exoclick.com', ''))) {
        //console.log(script_name, colors.red('The url of the request is matching. Aborting: ' + short_url));
        networkRequest.abort();
    }
    else if (requestData.url.match(new RegExp('http://ads2.contentabc.com', ''))) {
        //console.log(script_name, colors.red('The url of the request is matching. Aborting: ' + short_url));
        networkRequest.abort();
    }
    else if (requestData.url.match(new RegExp('http://vip.adstatic.com', ''))) {
        //console.log(script_name, colors.red('The url of the request is matching. Aborting: ' + short_url));
        networkRequest.abort();
    }
    else if (requestData.url.match(new RegExp(';base64,', ''))) {
        // console.log(script_name, colors.red('The url of the request is matching. Aborting: ' + short_url));
        networkRequest.abort();
    }

    if (0 /*param.task.disable_img_js*/) {


        if ((/http:\/\/.+?.css/gi).test(requestData['url']) || requestData['Content-Type'] == 'text/css') {
            //console.log('The url of the request is matching. Aborting css: ' + requestData['url']);
            request.abort();
        }
        else if ((/http:\/\/.+?.js/gi).test(requestData['url']) || requestData['Content-Type'] == 'application/javascript') {
            //console.log('The url of the request is matching. Aborting js: ' + requestData['url']);
            request.abort();
        }
        else if ((/http:\/\/.+?.jpg/gi).test(requestData['url']) || requestData['Content-Type'] == 'image/jpeg') {
            //console.log('The url of the request is matching. Aborting jpg: ' + requestData['url']);
            request.abort();
        }
        else if ((/http:\/\/.+?.jpeg/gi).test(requestData['url']) || requestData['Content-Type'] == 'image/pjpeg') {
            //console.log('The url of the request is matching. Aborting jpeg: ' + requestData['url']);
            request.abort();
        }
        else if ((/http:\/\/.+?.gif/gi).test(requestData['url']) || requestData['Content-Type'] == 'image/gif') {
            //console.log('The url of the request is matching. Aborting gif: ' + requestData['url']);
            request.abort();
        }
        else if ((/http:\/\/.+?.png/gi).test(requestData['url']) || requestData['Content-Type'] == 'image/png') {
            //console.log('The url of the request is matching. Aborting png: ' + requestData['url']);
            request.abort();
        }
        else if ((/http:\/\/.+?.tif/gi).test(requestData['url']) || requestData['Content-Type'] == 'image/tiff') {
            //console.log('The url of the request is matching. Aborting png: ' + requestData['url']);
            request.abort();
        }

    }

    /*
     if (requestData.url.match(new RegExp('.js$', ''))) {
     console.log(colors.red('The url of the request is matching. Aborting: ' + requestData.url));
     networkRequest.abort();
     }
     */


    //obj.freeze

    //if(config.const.debug == true){
    var tmp = requestData.url.split(".");
    var ext = tmp[tmp.length - 1];
    if (ext != 'gif' && ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'js' && ext != 'css'
        && requestData.url.indexOf('data:image/png;base64') == -1
    ) {

        // if page >1 lost user_agent
        //debug.log(script_name, ' ------------------------------------------------------------------ ');
        //debug.log(script_name, colors.green(' = ', 'Event:', 'Requested in page:', obj.id, requestData.url)); // JSON.stringify(obj.settings)
        //
        //if (url_actions[obj.id]) {
        //    //url_actions[obj.id].requested = requestData.url;
        //
        //    for (var id in requestData.headers) {
        //        if (requestData.headers.hasOwnProperty(id)) {
        //
        //            if (requestData.headers[id].name.toLowerCase() == 'referer') {
        //                //debug.log(' = ', 'Referer: ', requestData.headers[id].value);
        //                //url_actions[obj.id].refer_url    = requestData.headers[id].value;
        //                //url_actions[obj.id].refer_domain = domain.getDomain(requestData.headers[id].value);
        //                //param.referer[obj.id]            = requestData.headers[id].value;
        //            }
        //
        //            if (requestData.headers[id].name.toLowerCase() == 'user-agent') {
        //                //debug.log(script_name, 'DEBUG URL >>:', obj.id, 'user-agent', requestData.headers[id].value);
        //            }
        //
        //        }
        //    }
        //
        //}

        //debug.log(' = JS navigator.userAgent = ', obj.evaluate(function() { return navigator.userAgent; }) );
        //debug.log(' = JS window.navigator.userAgent = ', obj.evaluate(function() { return window.navigator.userAgent; }) );
        //debug.log(' = JS screen = ', obj.evaluate(function() { return 'screen.width = ' + screen.width+' screen.height= '+screen.height; }) );
        //debug.log(script_name, ' ------------------------------------------------------------------ ');


    }
    //}
    //debug.log(script_name, 'Event:', 'Requested in page:', obj.id, requestData.url);
    obj.requests++;

    //debug.log(script_name, 'onResourceRequested end');

}

function onResourceReceived(obj, response) {

    var tmp = response.url.split(".");
    var ext = tmp[tmp.length - 1];
    if (ext != 'gif' && ext != 'jpg' && ext != 'png' && ext != 'jpeg' && ext != 'js' && ext != 'css'
        && response.url.indexOf('data:image/png;base64') == -1
    ) {

        //debug.log(script_name, 'DEBUG URL <<:', obj.id, response.status, response.url);

        if (url_actions[obj.id]) {
            //url_actions[obj.id].received = response.url;

            url_actions[obj.id].code = response.status;

            for (var id in response.headers) {
                if (response.headers.hasOwnProperty(id)) {
                    if (response.headers[id].name.toLowerCase() == 'location') {
                        //debug.log(script_name, 'DEBUG URL <<:', obj.id, 'Location', response.headers[id].value);
                        url_actions[obj.id].redirect.push({
                            refer: response.url,
                            url:   response.headers[id].value
                        });
                        /*url_actions[obj.id].url = response.headers[id].value;
                         url_actions[obj.id].domain = domain.getDomain(response.headers[id].value);*/
                    }
                }
            }

        }

        /*console.log(obj.content);
         for(var id in response.headers){
         if(response.headers.hasOwnProperty(id)){
         debug.log(script_name, script_name, response.headers[id].name, response.headers[id].value);
         }
         }*/
    }

    //if(response.status != 200)
    //    debug.log(script_name, 'Event:', 'Received in page:', obj.id, 'status:', response.status, 'url:', response.url);
}

function check_domain(obj) {

    if (!param.task.tds) {

        debug.log(script_name, 'set tds domain', obj.domain);
        //console.log(script_name, 'set tds domain',obj.domain);
        param.task.tds = obj.domain;

    } else if (!param.task.site) {

        debug.log(script_name, 'set site domain', obj.domain);
        //console.log(script_name, 'set site domain',obj.domain);
        param.task.site = obj.domain;

    } else if (obj.domain == param.task.site) {

        debug.log(script_name, 'skip domain', obj.domain);
        //console.log(script_name, 'skip domain',obj.domain);

    } else {

        debug.log(script_name, 'Set trader domain', obj.domain);
        //console.log(script_name, 'Set trader domain',obj.domain);
        param.task.trader = obj.domain;

    }

}

function onUrlChanged(obj, targetUrl) {

    debug.log(script_name, 'onUrlChanged start targetUrl = ', targetUrl);

    //console.log('0');
    /*if(targetUrl.indexOf('http://no.task/') >= 0){
     throw new Error('NO_TASK');
     }*/

    //console.log('1');
    obj.changed++;
    obj.pageLoadedOk   = false;
    param.popupLoading = true;

    //console.log('2');
    obj.domain = domain.getDomain(targetUrl);

    //console.log('3');
    //url_actions[obj.id].url    = targetUrl;
    //url_actions[obj.id].domain = domain.getDomain(targetUrl);

    //console.log('3.1');
    debug.log(script_name, colors.bgRed.white('onUrlChanged() ', 'page.id: ', obj.id, 'targetUrl: ', targetUrl));
    //debug.log(script_name, colors.black.bgCyan('onUrlChanged() ', 'referer: ', param.referer[obj.id]));


    //console.log('4');
    //addToHistory({url: targetUrl, page: obj.id});


    //console.log('5');

    /*if(param.task.task_type && param.task.task_type.substr(0,7) == 'trader.'){
     if(param.task.trader && obj.domain != param.task.trader && obj.domain != param.task.site && targetUrl != 'about:blank' && obj.domain != '185.31.210.60'){
     debug.log(script_name, 'Event:', 'STOP', obj.id, targetUrl);
     obj.stop();
     }
     }*/

    console.log('6');
    debug.log(script_name, 'onUrlChanged end');
}

function onLoadFinished(obj, status) {

    obj.pageLoadedOk = true;
    debug.log(script_name, 'onLoadFinished, obj.pageLoadedOk = true');

    if (url_actions[obj.id]) {

        var log = {
            page_id:      url_actions[obj.id].page_id,
            code:         url_actions[obj.id].code,
            url:          url_actions[obj.id].url,
            domain:       url_actions[obj.id].domain,
            refer_url:    url_actions[obj.id].refer_url,
            refer_domain: url_actions[obj.id].refer_domain,
            createdAt:    url_actions[obj.id].createdAt,
            content:      obj.content.length,
            redirect:     url_actions[obj.id].redirect
        };
        param.url_log.push(log);
        //debug.json(JSON.stringify(log));
        url_actions[obj.id].code         = 0;
        url_actions[obj.id].url          = '';
        url_actions[obj.id].domain       = '';
        url_actions[obj.id].refer_url    = '';
        url_actions[obj.id].refer_domain = '';
        url_actions[obj.id].redirect     = [];

    }

    debug.log(script_name, 'onLoadFinished, 2');
    if (!param.task.trader) {

        //check_domain(obj);

    } else {

        if (timers[obj.id]) {
            clearTimeout(timers[obj.id])
            delete timers[obj.id];
        }
        debug.log(script_name, 'onLoadFinished, obj.freeze = true');
        obj.freeze = true;
        //obj.navigationLocked = true;

        debug.log(script_name, 'onLoadFinished, param.popupLoading = false');
        param.popupLoading = false;

        debug.log(script_name, 'Event:', 'LoadFinished', obj.id, 'requests:', obj.requests, 'content len:', obj.content.length);
        //debug.log(script_name, obj.content);

        var content_short = obj.content.substr(0, 512) + '...';
        debug.log();
        debug.log(script_name, 'content:', 'page_id: ', obj.id, 'url: ', obj.url);
        debug.log(script_name, '==================================================================================');
        debug.log(script_name, colors.bgBlack(content_short));
        debug.log(script_name, '==================================================================================');
        debug.log();


    }

    debug.log(script_name, 'onLoadFinished, 3');
}

function onClosing(closingPage) {

    debug.log(script_name, 'Event:', 'Closing', closingPage.id, closingPage.url);

}

function onConsoleMessage(obj, msg, lineNum, sourceId) {

    debug.log(script_name, colors.inverse.gray(script_name, '--> ConsoleMessage:', 'page_id: ', obj.id, 'url: ', obj.url, ' lineNum:', lineNum, ' sourceId: ', sourceId, 'message: ', JSON.stringify(msg)));

}

function getQueryVariable(query, variable) {
    var vars = query.split("?")[1].split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) {
            return pair[1];
        }
    }
}

function qsParseArray(query) {
    var res  = [];
    var vars = query.split("?")[1].split("&");
    for (var i = 0; i < vars.length; i++) {
        res.push(vars[i].split("="));
    }
    return res;
}




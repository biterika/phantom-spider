url1 = 'https://freesafeip.com:888/df/index.php';
url2 = 'http://freesafeip.com/df';

function url_compare(url1, url2) {
    if (url1 && url2) {

        function cut_index_postfix (url) {
            var cut_patterns = ['index.shtml','index.php','index.html'];
            for (var p in cut_patterns) {
                if (url.indexOf(cut_patterns[p]) > 0) {
                    url = url.substr(0, url.indexOf(cut_patterns[p]));
                    break;
                }
            }
            return url;
        }
        url1 = cut_index_postfix(url1);
        url2 = cut_index_postfix(url2);

        var url1_match = url1.match(/^(https?)\:\/\/(www[0-9]?\.)?(.[^:\/?]+)?:?([0-9]{0,}[^:\/?])?([\/].{0,}[^:\/?])?/);
        var url2_match = url2.match(/^(https?)\:\/\/(www[0-9]?\.)?(.[^:\/?]+)?:?([0-9]{0,}[^:\/?])?([\/].{0,}[^:\/?])?/);

        if (url1_match && url2_match && url1_match[3] && url2_match[3]) {
            var url1_compare = (url1_match[3] + url1_match[5]);
            var url2_compare = (url2_match[3] + url2_match[5]);

            console.log('url1_compare', url1_compare);
            console.log('url2_compare', url2_compare);

            return url1_compare == url2_compare;

        } else {
            return false
        }
    } else {
        return false
    }
}



console.log(url1,url2);

console.log(url_compare( url1,url2));
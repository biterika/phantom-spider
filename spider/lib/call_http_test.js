var http = require('http');

var options = {
    host: 'smartcj.biterika.com',
    path: '/',
    port: '80',
    //This is the only line that is new. `headers` is an object with the headers to request
    headers: {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36'}
};

callback = function(response) {
    var str = ''
    response.on('data', function (chunk) {
        str += chunk;
    });

    response.on('end', function () {
        console.log('http answer'+str);
    });
}

var req = http.request(options, callback);
req.end();
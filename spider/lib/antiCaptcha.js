'use strict';

var createTask_RETRY_LIMITS = 3;

var getTaskResult_RETRY_INTERVAL = 1*1000;
var getTaskResult_RETRY_LIMITS = 120;

var HTTP_FINISH_CHECK_INTERVAL = 1*1000;
var HTTP_FINISH_CHECK_RETRY_LIMITS = 60;

var api_anti_captcha_key = 'bb8964e192c9465e98a7793a03c6655d';

var page = require('webpage').create();
page.settings.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.116 Safari/537.36';

page.settings.resourceTimeout               = 0;
page.settings.javascriptEnabled             = false;
page.settings.loadImages                    = false;
page.settings.localToRemoteUrlAccessEnabled = false;
page.settings.webSecurityEnabled            = false;

page.onResourceRequested = function (requestData, request) {
    if ((/http:\/\/.+?.css/gi).test(requestData['url']) || requestData['Content-Type'] == 'text/css') {
        console.log('The url of the request is matching. Aborting: ' + requestData['url']);
        request.abort();
    }
};

/*
* return in callback false or text of captcha
* */
function request(captcha_ImgBase64, captcha, callback, retry_cnt) {

    console.log('antiCaptcha request captcha_ImgBase64 = '+captcha_ImgBase64+' captcha = '+captcha+' retry_cnt = '+retry_cnt);

    if (!retry_cnt) retry_cnt = 0;
    if (retry_cnt > createTask_RETRY_LIMITS) {
        console.error('fail, reaching createTask_RETRY_LIMITS');
        if (callback) {
            callback({status: 'fail', fn: 'request', retry_cnt: retry_cnt});
            callback = null;
            return;
        }
    }

    if (captcha.anticaptcha_key) {
        api_anti_captcha_key = captcha.anticaptcha_key;
    }

    console.log(' api_anti_captcha_key = ',api_anti_captcha_key);
    console.log(' captcha = ',JSON.stringify(captcha));

    var taskId = null;

    var captcha_task = {
        "clientKey": api_anti_captcha_key,
        "task":      {
            "type":      "ImageToTextTask",
            "body":      captcha_ImgBase64,
            "phrase":    false,
            "case":      false,
            "numeric":   false,
            "math":      0,
            "minLength": 0,
            "maxLength": 0
        }
    };

    console.log('captcha_task ',JSON.stringify(captcha_task));

    post(// 'http://192.168.99.1:2323',
        'https://api.anti-captcha.com/createTask',captcha_task
        ,
        function result(res) {

            console.log('createTask result ' + JSON.stringify(res));
            // {"status":"success","resp":{"errorId":0,"taskId":16229675}}

            if (res.status && res.status == 'success' && res.resp && res.resp.taskId) {
                taskId = res.resp.taskId;
                console.log('createTask success taskId = ' + taskId);
                getTaskResult(taskId, consumeTaskResult);

            }
            else {
                retry_cnt++;
                console.log('taskCapcha retry ' + retry_cnt + ' ... ');
                return request(captcha_ImgBase64, null, callback, retry_cnt);
            }
        }
    );

    function getTaskResult(taskId, callback_gtr, retry_cnt_gtr) {
        console.log('getTaskResult start');
        if (!retry_cnt_gtr) retry_cnt_gtr = 0;
        if (retry_cnt_gtr > getTaskResult_RETRY_LIMITS) {
            if (callback_gtr) {
                callback_gtr({status: 'fail', fn: 'getTaskResult', retry_cnt: retry_cnt});
                callback_gtr = null;
                return;
            }
        }

        console.log('antiCaptch post https://api.anti-captcha.com/getTaskResult');
        post('https://api.anti-captcha.com/getTaskResult',
            {
                "clientKey": api_anti_captcha_key,
                "taskId":    taskId
            },
            callback_int
        );

        function callback_int(res) {
            if (callback_gtr) {
                callback_gtr(res, retry_cnt_gtr);
                callback_gtr = null;
            }
        }

    }

    function consumeTaskResult(res, retry_cnt_gtr) {

        console.log('getTaskResult result ' + JSON.stringify(res)+'  retry_cnt_gtr = '+retry_cnt_gtr);
        /*
        {"errorId":0,"status":"ready","solution":{"text":"Earth","url":"http:\/\/69.39.239.239\/679\/147541268486992.png"},"cost":"0.001000","ip":"163.172.162.7","createTime":1475412684,"endTime":1475412740,"solveCount":"0"}
getTaskResult result {"status":"success","resp":{"errorId":0,"status":"ready","solution":{"text":"Earth","url":"http://69.39.239.239/679/147541268486992.png"},"cost":"0.001000","ip":"163.172.162.7","createTime":1475412684,"endTime":1475412740,"solveCount":"0"}}
*/
        if (res.errorId && res.errorId !== 0) {
            console.error('getTaskResult responde with error ' + JSON.stringify(res));
            if (callback) {
                callback({status: 'fail', fn: 'getTaskResult', details: res});
                callback = null;
            }
        }

        else if (res.status && res.resp.status == 'processing') {
            setTimeout(function (retry_cnt_gtr) {
                retry_cnt_gtr++;
                console.log('getTaskResult retry ' + retry_cnt_gtr + ' ...');
                getTaskResult(taskId, consumeTaskResult, retry_cnt_gtr);
            }, getTaskResult_RETRY_INTERVAL, retry_cnt_gtr);

        }

        else if (res.status && res.resp.status == 'ready' && res.resp.solution && res.resp.solution.text) {
            if (callback) {
                callback({status: 'success', answer: res.resp.solution.text});
                callback = null;
            }
        }

        else {
            console.log('getTaskResult unknown response ' + JSON.stringify(res));
            if (callback) {
                callback({status: 'fail', fn: 'getTaskResult', details: res});
                callback = null;
            }
        }

    }

}
exports.request = request;


function post(url, data_obj, callback) {

    console.log('post start url = ' + url);

    var data = JSON.stringify(data_obj);

    //console.log('send postData '+data);

    page.open(url, 'post', data, function (status) {
        if (status !== 'success') {
            console.log('post: Unable to post! ' + status);

            if (callback) {
                callback({status: 'fail'});
                callback = null;
            }
        } else {
            console.log('post: success');
        }
    });


    check();

    function check(cnt) {
        if (!cnt) cnt = 0;
        if (!callback) return;

        if (page.loading) {

            console.log(cnt + ' page.loading = ' + page.loading + ' page.loadingProgress= ' + page.loadingProgress);

            setTimeout(function (cnt) {
                cnt++;
                check(cnt);
            }, HTTP_FINISH_CHECK_INTERVAL, cnt);

        }
        else if (cnt > HTTP_FINISH_CHECK_RETRY_LIMITS) {

            if (callback) {
                callback({status: 'fail'});
                callback = null;
            }

        }
        else if (page.plainText) {

            console.log('load finished');
            console.log('load finished page.plainText ' + page.plainText);
            //callback(page.plainText);

            var resp_obj = null;
            try {
                resp_obj = JSON.parse(page.plainText);
            } catch (e) {
                console.error('post: error parse json');
            }

            if (resp_obj && callback) {
                /*
                {
                  "errorId": 0,
                  "taskId": 16229675
                */

                if (!resp_obj.errorId || resp_obj.errorId == 0) {
                    if (callback) {
                        callback({status: 'success', resp: resp_obj});
                        callback = null;
                    }
                } else {
                    if (callback) {
                        callback({status: 'fail', error: resp_obj});
                        callback = null;
                    }
                }
            }

        }
        else {
            console.log(cnt + ' (!page.plainText) page.loading = ' + page.loading + ' page.loadingProgress= ' + page.loadingProgress);

            setTimeout(function (cnt) {
                if (callback) {
                    cnt++;
                    check(cnt);
                }
            }, HTTP_FINISH_CHECK_INTERVAL, cnt);

        }

    }

}
exports.post = post;

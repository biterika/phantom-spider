var param   = require('./../inc/param');

var time_start = parseInt(new Date().getTime()/1000);
var fs = require('fs');

var time = function(){
    var d = new Date();
    var h = d.getHours();if (h <= 9) h = "0" + h;
    var m = d.getMinutes();if (m <= 9) m = "0" + m;
    var s = d.getSeconds();if (s <= 9) s = "0" + s;
    return h+':'+m+':'+s;
};
var time_delta = function(){
    var time_now = parseInt(new Date().getTime()/1000);
    return time_now - time_start;
};
var time_int = function(){
    return parseInt(new Date().getTime()/1000)
};

function log(){
    var text = '';
    for (i = 0; i < arguments.length; i++) {
        text += (i > 0 ? ' ' + arguments[i] : arguments[i]);
    }

    var message = time()+" "+time_delta()+" "+'Step:'+" "+param.step+" "+text;

    console.log(message);

    if (param.write_task_log) {
        if (param.hasOwnProperty('task')) {
            if (param.task.hasOwnProperty('task_id')) {


                var log_dir = fs.absolute('./logs'); //sys.args[3]

                if ( !fs.isDirectory(log_dir) ) {
                    console.log('creating dir: '+log_dir);
                    fs.makeDirectory(log_dir);
                }

                var path = log_dir + '/' + param.task.task_id + '.log';
                //console.log('log ' + path);
                fs.write(path, message + '\n', 'a');
            }
        }
    }

}

exports.log = log;

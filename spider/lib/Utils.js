module.exports = {
// var uri_out = 'https://www.fpcTraffic3.com/raw/click.cgi?account=biterika&track=A&backurl=http://agrannyporn.net/return';

    getHostName: function (url) {
        if (url) {


            var match = url.match(/^(https?)\:\/\/(www[0-9]?\.)?(.[^://?]+)/);
             console.log(match);
            if (match != null && match.length > 2 && typeof match[3] === 'string' && match[3].length > 0) {

                if (match[1] == 'http' || match[1] == 'https') return match[3].toLowerCase();
            }
        }
        return false;
    },

    urlCompare: function(url1,url2){
        if (url1 && url2) {

            function cut_index_postfix (url) {
                var cut_patterns = ['index.shtml','index.php','index.html','index.htm','index.asp','en/','en'];
                for (var p in cut_patterns) {
                    if (url.indexOf(cut_patterns[p]) > 0) {
                        url = url.substr(0, url.indexOf(cut_patterns[p]));
                        break;
                    }
                }
                return url;
            }
            url1 = cut_index_postfix(url1);
            url2 = cut_index_postfix(url2);


            //console.log('url1', url1);
            //console.log('url2', url2);

            var url1_match = url1.match(/^(https?)\:\/\/(www[0-9]?\.)?(.[^:\/?]+)?:?([0-9]{0,}[^:\/?])?([\/].{0,}[^:\/?])?/);
            var url2_match = url2.match(/^(https?)\:\/\/(www[0-9]?\.)?(.[^:\/?]+)?:?([0-9]{0,}[^:\/?])?([\/].{0,}[^:\/?])?/);

            //console.log('url1_match', url1_match);
            //console.log('url2_match', url2_match);

            if (url1_match && !url1_match[5]) url1_match[5]= '';
            if (url2_match && !url2_match[5]) url2_match[5]= '';

            if (url1_match && url2_match && url1_match[3]) {
                var url1_compare = (url1_match[3] + url1_match[5]);
                var url2_compare = (url2_match[3] + url2_match[5]);

                //console.log('url1_compare', url1_compare);
                //console.log('url2_compare', url2_compare);

                return url1_compare == url2_compare;

            } else {
                return false
            }
        } else {
            return false
        }

     }

};



exports.char = function(cnt) {
    var result       = '';
    var words        = '0123456789abcdef';
    var max_position = words.length - 1;
    for( i = 0; i < cnt; ++i ) {
        position = Math.floor ( Math.random() * max_position );
        result = result + words.substring(position, position + 1);
    }
    return result;
};

exports.int = function(min,max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
};

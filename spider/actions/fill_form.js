var config  = require('./../inc/config');
var param   = require('./../inc/param');
var random  = require('./../lib/random');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var script_name = 'fill_form() ';

exports.exec = function(obj,form_values, submit){
    debug.log(script_name, 'start');

    if(typeof obj != 'undefined' && obj){
        var result = obj.evaluate(function(form_values, submit) {

            // ///////////
            //if (!opt) {opt='all';}

            console.log('arg form_values for fill = ',JSON.stringify(form_values));
            console.log('arg submit = ',JSON.stringify(submit));

            var form_elem, input_elems, input_elem, textarea_elems;
            var trade_form_data = [];
            var tmp_form_fields_data =[];
            var tmp_form_textarea_data =[];
            var tmp_fields_count = 0;
            var form_elems = document.querySelectorAll('form');
            for (var i = 0; i < form_elems.length; i++) {

                tmp_form_fields_data =[];
                tmp_form_textarea_data =[];
                tmp_fields_count = 0;

                input_elems = form_elems[i].querySelectorAll('input');

                for (var i2 = 0; i2 < input_elems.length; i2++) {
                    if (input_elems[i2].type!=='hidden' && input_elems[i2].type!=='submit') {
                        tmp_fields_count++;
                        tmp_form_fields_data.push({
                            tag_name: 'input',
                            tag_index: i2,
                            name: input_elems[i2].name,
                            type: input_elems[i2].type,
                            placeholder: input_elems[i2].placeholder,
                            id: input_elems[i2].id,
                            class: input_elems[i2].className
                        });
                    }
                }

                textarea_elems = form_elems[i].querySelectorAll('textarea');

                for (var i3 = 0; i3 < textarea_elems.length; i3++) {
                    tmp_fields_count++;
                    tmp_form_fields_data.push({
                        tag_name: 'textarea',
                        tag_index: i3,
                        name: textarea_elems[i3].name,
                        type: textarea_elems[i3].type,
                        placeholder: textarea_elems[i3].placeholder,
                        id: textarea_elems[i3].id,
                        class: textarea_elems[i3].className
                    });
                }

                trade_form_data.push({
                    form: {
                        tag_name: 'form',
                        tag_index: i,
                        fields_amount: tmp_fields_count,
                        name: input_elems[i].name,
                        method: form_elems[i].method,
                        id: form_elems[i].id,
                        class: form_elems[i].className
                    },
                    fields: tmp_form_fields_data,
                });
            }

            var max_fields_amounts=0;
            var form_idx_with_max_fields_amount;
            for (var form_idx in trade_form_data) {
                //console.log(form_idx,trade_form_data[form_idx].form.fields_amount);
                if (trade_form_data[form_idx].form.fields_amount > max_fields_amounts) {
                    //console.log('max form_idx',form_idx);
                    max_fields_amounts = trade_form_data[form_idx].form.fields_amount;
                    form_idx_with_max_fields_amount = form_idx;
                }
            }


//return {form_data: trade_form_data[form_idx_with_max_fields_amount].fields,verbose_form_data: trade_form_data};
            if (form_idx_with_max_fields_amount) {
                var main_form_el= form_elems[form_idx_with_max_fields_amount];

                if (submit) {
                    // main_form_el.submit();

                    // var inputs = document.getElementsByTagName('input');
                    var inputs = main_form_el.getElementsByTagName('input');
                    var sumbmits = [];
                    for (var i in inputs) {
                        if (inputs[i] && inputs[i].type && (inputs[i].type=='submit' || inputs[i].type=='button')) {sumbmits.push(inputs[i]); }
                    }

                    console.log('founded sumbmits len = '+ sumbmits.length);
                    console.log(sumbmits);

                    //var mouseover = new MouseEvent('mouseover', {'view': window, 'bubbles': true, 'cancelable': true});
                    //var mousedown = new MouseEvent('mousedown', {'view': window, 'bubbles': true, 'cancelable': true});
                    //var mouseup   = new MouseEvent('mouseup', {'view': window, 'bubbles': true, 'cancelable': true});
                    //var click     = new MouseEvent('click', {'view': window, 'bubbles': true, 'cancelable': true});

                    if (sumbmits[0]) {
                        console.log('emulate click on submit');
                        var target_element = sumbmits[0];

                        //target_element.dispatchEvent(mouseover);
                        //target_element.dispatchEvent(mousedown);
                        //target_element.dispatchEvent(mouseup);
                        //target_element.dispatchEvent(click);
                        //target_element.focus();
                        var ev = document.createEvent("MouseEvent");
                        var el = sumbmits[0];
                        // el.setAttribute('target', '_blank');
                        ev.initMouseEvent(
                            "click",
                            true /* bubble */, true /* cancelable */,
                            window, null,
                            0, 0, 0, 0, /* coordinates */
                            false, false, false, false, /* modifier keys */
                            0 /*left*/, null
                        );

                        el.dispatchEvent(ev);

                    } else {
                        console.log('submit not found');
                        main_form_el.submit();
                    }
                    //main_form_el.submit();

                } else {

                    var field_selector, field_value, field_el;
                    for (var fv_idx in form_values) {
                        field_selector = form_values[fv_idx].selector;
                        field_value = form_values[fv_idx].value;
                        //console.log();
                        if (field_selector) {
                            console.log('querySelectorAll arg = ',field_selector);
                            field_el = main_form_el.querySelectorAll(field_selector)[0];
                            if (field_el) {
                                console.log('querySelectorAll arg = ',field_selector,' field success selected');
                                if (field_el.type == 'checkbox') {
                                    if (
                                        (field_value == 'true' && !field_el.checked) ||
                                        (field_value == 'false' && field_el.checked)
                                    ) {
                                        var ev = document.createEvent("MouseEvent");
                                        field_el.setAttribute('target', '_self');
                                        ev.initMouseEvent(
                                            "click",
                                            true /* bubble */, true /* cancelable */,
                                            window, null,
                                            0, 0, 0, 0, /* coordinates */
                                            false, false, false, false, /* modifier keys */
                                            0 /*left*/, null
                                        );
                                        field_el.dispatchEvent(ev);
                                    }

                                }
                                else {
                                    field_el.value = field_value;
                                }
                            } else {
                                console.log('field selecting error, field_selector = ',field_selector);
                            }
                        } else {
                            console.log('field selector incorrect, field_selector = ',field_selector);
                        }
                    }
                    // console.log(form_idx_with_max_fields_amount);

                }
            }
            return true;


            // ///////////


        },form_values, submit);
    } else {
        debug.log(script_name, 'obj error');
    }

    debug.log(script_name, 'end');
    return result;



};

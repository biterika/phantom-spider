var config  = require('./../inc/config');
var param   = require('./../inc/param');
var random  = require('./../lib/random');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var script_name = 'get_all_links() ';

exports.exec = function(obj,opt){
    debug.log(script_name, 'start');
    //debug.log(script_name, 'get_all_links page.plainText',page.content);
    var links_all; //= [];
    if(typeof obj != 'undefined' && obj){
        links_all = obj.evaluate(function(opt) {

            // ///////////
            if (!opt) {opt='all';} // var


            var links_arr = [];

            var links_elems = document.querySelectorAll('a');
            //console.log( links_elems.length,  links_elems.length);
            for (var i = 0; i < links_elems.length; i++) {

                if (opt=='with_innerText') {
                    if (links_elems[i].innerText) {
                        //console.log(links_elems[i].innerText, '  ', links_elems[i].href);
                        links_arr.push({innerText: links_elems[i].innerText, href:  links_elems[i].href})
                    }
                    else if (links_elems[i].querySelectorAll('img').length>0 &&
                        links_elems[i].querySelectorAll('img')[0].alt)
                    {

                            links_arr.push({
                                innerText: links_elems[i].querySelectorAll('img')[0].alt,
                                href:      links_elems[i].href
                            })
                    }

                } else if (opt=='all') {
                        links_arr.push(links_elems[i].href);
                } else {
                    return false;
                }

            }




            //console.log(links_arr);
            return links_arr;

            // ///////////


        },opt);
    } else {
        debug.log(script_name, 'obj error');
    }

    /*if(links_all.length < 1) {

        debug.log('links not found');
        return false;
    }
    else{
*/
    debug.log(script_name, 'end');
        return links_all;

    /*}*/

};

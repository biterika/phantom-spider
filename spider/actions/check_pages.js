var config = require('./../inc/config');
var param  = require('./../inc/param');
var debug  = require('./../lib/debug');
var Q      = require('./../modules/q/q.js');
var colors = require('./../modules/colors/safe.js');

exports.exec = function (obj) {

    var page_for_click    = obj;
    var page_for_click_id = obj.id;
    var click             = false;
    var click_content     = false;

    if (param.task.task_type_origin == 'click') {

        debug.log('check_pages() ', 'close other pages');
        debug.log('check_pages() ', 'param.pages_for_close = ', JSON.stringify(param.pages_for_close));


        //close other pages
        if (param.pages_for_close.length > 0) {

            for (var id in obj.pages) {
                if (obj.pages.hasOwnProperty(id)) {
                    if (obj.pages[id].hasOwnProperty('id')) {

                        for (var page_id in param.pages_for_close) {

                            debug.log('for param.pages_for_close page_id=', param.pages_for_close[page_id]);

                            if (obj.pages[id].id == param.pages_for_close[page_id]) {

                                debug.log('  ', 'check_pages()', colors.bgRed.black('Page id :', param.pages_for_close[page_id], 'close()'));
                                obj.pages[id].close();
                                delete param.pages_for_close[page_id];

                                //for (var page_id2 in param.pages_for_close) {
                                //    if (param.pages_for_close[page_id2] == param.pages_for_close[page_id]) {
                                //        console.log('delete param.pages_for_close[page_id2]=',param.pages_for_close[page_id2],' page_id2=',page_id2);
                                //        delete param.pages_for_close[page_id2];
                                //    }
                                //}
                                //break;
                            }
                        }
                    }
                }
            }

        }


        param.pages_for_close = cleanArray(param.pages_for_close);

        debug.log('check_pages() ', 'close other finished');
    }

    /*if(click)
        param.task.click_ok++;

    if(click_content)
        param.task.click_content++;*/

    return page_for_click;

};


function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
        if (actual[i]) {
            newArray.push(actual[i]);
        }
    }
    return newArray;
}
opt = 'all';

var form_elem, input_elems, input_elem, textarea_elems;
var trade_form_data        = [];
var tmp_form_fields_data   = [];
var tmp_form_textarea_data = [];
var tmp_fields_count       = 0;
var tmp_total_fields_count = 0;
var form_elems             = document.querySelectorAll('form');
for (var i = 0; i < form_elems.length; i++) {

    tmp_form_fields_data   = [];
    tmp_form_textarea_data = [];
    tmp_fields_count       = 0;

    input_elems = form_elems[i].querySelectorAll('input');

    for (var i2 = 0; i2 < input_elems.length; i2++) {
        if (input_elems[i2].type !== 'hidden' && input_elems[i2].type !== 'submit') {
            tmp_fields_count++;
            tmp_total_fields_count++;
            if (input_elems[i2]) {
                tmp_form_fields_data.push({
                    tag_name:    'input',
                    tag_index:   i2,
                    name:        input_elems[i2].name,
                    type:        input_elems[i2].type,
                    placeholder: input_elems[i2].placeholder,
                    id:          input_elems[i2].id,
                    class:       input_elems[i2].className
                });
            }
        }
    }

    textarea_elems = form_elems[i].querySelectorAll('textarea');

    for (var i3 = 0; i3 < textarea_elems.length; i3++) {
        tmp_fields_count++;
        tmp_total_fields_count++;

        if (textarea_elems[i3]) {
            tmp_form_fields_data.push({
                tag_name:    'textarea',
                tag_index:   i3,
                name:        textarea_elems[i3].name,
                type:        textarea_elems[i3].type,
                placeholder: textarea_elems[i3].placeholder,
                id:          textarea_elems[i3].id,
                class:       textarea_elems[i3].className
            });
        }
    }

    if (input_elems[i]) {
        trade_form_data.push({
            form:   {
                tag_name:      'form',
                tag_index:     i,
                fields_amount: tmp_fields_count,
                name:          input_elems[i].name,
                method:        form_elems[i].method,
                id:            form_elems[i].id,
                class:         form_elems[i].className
            },
            fields: tmp_form_fields_data,
        });
    }
}

if (tmp_total_fields_count > 0) {
    var max_fields_amounts = 0;
    var form_idx_with_max_fields_amount;
    for (form_idx in trade_form_data) {
        //console.log(form_idx,trade_form_data[form_idx].form.fields_amount);
        if (trade_form_data[form_idx].form.fields_amount > max_fields_amounts) {
            //console.log('max form_idx',form_idx);
            max_fields_amounts              = trade_form_data[form_idx].form.fields_amount;
            form_idx_with_max_fields_amount = form_idx;
        }
    }

    /*    return {
            form_data:         trade_form_data[form_idx_with_max_fields_amount].fields,
            verbose_form_data: trade_form_data
        };*/

    console.log({
        form_data:         trade_form_data[form_idx_with_max_fields_amount].fields,
        verbose_form_data: trade_form_data
    });
}
else {
    /*    return {
            form_data:         [],
            verbose_form_data: []
        };*/
    console.log({
        form_data:         [],
        verbose_form_data: []
    });
}
var system = require('system');

var param  = require('./../inc/param');
var config = require('./../inc/config');

var domain = require('./../lib/domain');
var Utils  = require('./../lib/Utils');
var debug  = require('./../lib/debug');
var Q      = require('./../modules/q/q.js');
var colors = require('./../modules/colors/safe.js');

var events = require('./../events').exec;

var script_name = 'page_open()';

var obj          = null;
var result       = {};
var pageLoadedOk = false;

var exit = false;

exports.exec = function (_obj, target_url, make_new_page) {

    var deferred = Q.defer();

    Q.fcall(function () {
        if (make_new_page) {
            debug.log(script_name, '#1 make_new_page init() .... ');
            return init();
        } else {
            debug.log(script_name, '#1 use exsist page ... ');
            obj = _obj;
            return true;
        }

    }).delay(3000).then(function () {

        if (make_new_page) {
            debug.log(script_name, '#2 make_new_page events(obj) .... ');
            return events(obj);
        }
        return true;

    }).delay(3000).then(function () {
        param.step++;
        debug.log(script_name, '#3 start open page:', target_url);

        return openPage(target_url);

    }).catch(function (err) {
        console.error(script_name, 'catch err ' + JSON.stringify(err));
        exit = true;
        deferred.reject(err);
        //phantom.exit(11);

    }).delay(2000).done(function (page_new) {
        deferred.resolve(page_new);
        //deferred.resolve({status: 'success'});
    });

    return deferred.promise;
};


function init() {

    var deferred = Q.defer();

    debug.log(script_name, 'init() start');
    obj = webpage.create();

    if (param.task.disable_img_js) {
        //obj.settings.javascriptEnabled             = false;
        obj.settings.loadImages = false;
    }

    page.open('about:blank', function (status) {
        debug.log(script_name, 'init() about:blank', status);
        debug.log(script_name, 'init() deferred.resolve(true);');
        deferred.resolve(true);
    });

    return deferred.promise;

}


function openPage(target_url) {

    var deferred = Q.defer();

    obj.open(target_url, function (status) {

        debug.log(script_name, 'resolving page status...');

        if (status == 'success') {
            debug.log(script_name, 'page opened success');
            pageLoadedOk = true;
            //deferred.resolve({status: 'success'});

        } else {
            debug.log(script_name, 'open page status', status, ' dont worry, its okay open.');
            pageLoadedOk = true;
            //deferred.resolve({status: 'success'});
            // deferred.reject({status: 'SITE_OPEN_FAIL'});

        }
    });


    obj.freeze = false;


    setTimeout(function () {
        check();
    }, 2000);


    var check_count = 0;

    function check() {

        setTimeout(function () {


            debug.log(script_name, colors.green.bgBlack('check()...', check_count, 'obj.url', obj.url, 'target_url', target_url, ' pageLoadedOk: ', pageLoadedOk, ' obj.loading = ' + obj.loading + ' obj.loadingProgress= ' + obj.loadingProgress));



            check_count++;
            if (obj.loadingProgress==100 && check_count>5) {
                debug.log('obj.loadingProgress==100 && check_count>15');
                if (obj.content && obj.content.length ==0 || obj.content=='<html><head></head><body></body></html>') {
                    debug.log(script_name, colors.red.bgWhite('PAGE_OPEN_EMPTY'));
                    result = {status: 'PAGE_OPEN_EMPTY'};
                    deferred.reject(result);
                } else {

                    debug.log('obj.loadingProgress==100 && check_count>15 ---->  finish()');
                    finish();
                }

            }
            else if (Utils.urlCompare(obj.url, target_url) && pageLoadedOk) {
                finish();
            }
            else if (obj.url && obj.url!=='about:blank' && domain.getDomain(obj.url) !== domain.getDomain(target_url)) {
                debug.log(script_name, colors.red.bgWhite('REDIRECT_TO_ANOTHER_DOMAIN'), '(1) obj.url = ',obj.url,' domain.getDomain(obj.url) ',domain.getDomain(obj.url), 'domain.getDomain(target_url)',domain.getDomain(target_url));
                result = {status: 'REDIRECT_TO_ANOTHER_DOMAIN'};
                deferred.reject(result);
            }
            else if (exit) {
                debug.log(script_name,'exit exit_case');
            }
            else {

                if (check_count > 60 && pageLoadedOk && domain.getDomain(obj.url) == domain.getDomain(target_url)) {
                    finish();
                }
                else if (check_count > 60 && pageLoadedOk) {
                    finish();
                }
                else if (check_count > 60) {

                    debug.log(script_name, colors.red.bgWhite('PAGE_OPEN_TIMEOUT'));
                    result = {status: 'PAGE_OPEN_TIMEOUT'};
                    deferred.reject(result);

                } else {
                    check();
                }

            }


        }, 2000);


    }

    function finish() {
        debug.log(script_name, colors.bold.bgWhite.magenta('Open page success', 'wait:', config.timeout.trader_after_open / 1000 + 's'));
        setTimeout(function () {
            result = {status: 'success'};
            deferred.resolve(obj);
        }, config.timeout.trader_after_open);
    }

    return deferred.promise;
}
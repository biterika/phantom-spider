var fs = require('fs');

//var time    = require('./../lib/time');
var config = require('./../inc/config');
var param  = require('./../inc/param');
var debug  = require('./../lib/debug');
var colors = require('./../modules/colors/safe.js');
var Q      = require('./../modules/q/q.js');
var node_ipc = require('./../actions/node_ipc').exec;

var detect_site_types = require('./../actions/detect_site_types').exec;
//var setTimeoutStop = require('./../actions/events').setTimeoutStop;

var script_name = 'task_finish_post()';


var data      = {};
var post_data = null;


exports.exec = function (page, task_results) {

    var deferred = Q.defer();


    Q.fcall(function () {
        param.step++;
        debug.log(script_name, 'start');


        //debug.log(script_name, 'Task finish:', task.code, 'Clicks:', task.click_ok, '/', task.click_task);


        debug.log(script_name, 'param.task.task_type_origin = ', param.task.task_type_origin);
        debug.log(script_name, 'param.task.task_type = ', param.task.task_type);
        /*task_finish_post()  catch:  {"stack":"phantomjs://platform/task_finish_post.js:35:40\napply@phantomjs://platform/q.js:1166:31\npromiseDispatch@phantomjs://platform/q.js:789:46\nphantomjs://platform/q.js:1392:29\nrunSingle@phantomjs://platform/q.js:138:17\nflush@phantomjs://platform/q.js:126:22","line":35,"sourceURL":"phantomjs://platform/task_finish_post.js"} */
        // detect site types

        /*task_finish_post() task_results =  {"TRADE_FORM_SEND":{"have":null,"array":{"form_url":null,"form_url_after_submit":null,"textPage_after_submit":null,"success_message":null,"result_success_message":null}}}
        * */
        debug.log(script_name, 'task_results = ', JSON.stringify(task_results));


        if (task_results.hasOwnProperty('CATEGORY_MATCH') && task_results.CATEGORY_MATCH.have) {
            debug.log(script_name, 'start detect_site_types');
            return detect_site_types(task_results.CATEGORY_MATCH.array);
        }


    }).delay(2000).then(function (site_types) {

        if (task_results.hasOwnProperty('CATEGORY_MATCH') && task_results.CATEGORY_MATCH.have && site_types) {
            debug.log(script_name, '<- [SITE_TYPES] [' + JSON.stringify(site_types) + ']');
            task_results.SITE_TYPES.array = site_types;
        }


        if (param.task.task_type_origin == 'click') {
            var urls = [];

            if (param.hasOwnProperty('history')) {
                if (param.history.hasOwnProperty('domains')) {
                    for (var domain_idx in param.history.domains) {
                        if (param.history.domains[domain_idx].hasOwnProperty('domain')) {
                            urls.push(param.history.domains[domain_idx].domain);
                        }
                    }
                }
            }

            data =
            {
                id:   param.task.task_id,
                urls: urls
            };

            /*if (param.hasOwnProperty('history')) {
                data.debug_data.history = param.history;
            }*/

        }
        else if (param.task.task_type_origin == 'discover') {


            debug.log(script_name, 'prepare data for discover scenario');

            debug.log(script_name, 'prepare data for discover scenario', JSON.stringify(task_results));


            var categories;
            if (task_results.CATEGORY_MATCH && task_results.CATEGORY_MATCH.have) {
                categories = task_results.CATEGORY_MATCH.array;
            } else {
                categories = ['undefined'];
            }

            var is_trade, trade_script, form_url, fields;
            if (task_results.TRADE_FORM && task_results.TRADE_FORM.have) {

                if (!param.task.closed_list_match) {
                    is_trade = 1;
                } else {
                    is_trade = 0;
                }

                fields = task_results.TRADE_FORM.array.fields;

                //debug.log('003');

                form_url = task_results.TRADE_FORM.array.link;

                //debug.log('004');

                //debug.log('004', form_url);

                if (form_url.match(new RegExp('\/ftt2\/', ''))) {
                    trade_script = 'ftt2';
                } else if (form_url.match(new RegExp('scjwebmaster', ''))) {
                    trade_script = 'scj';
                } else {
                    trade_script = 0;
                }

                debug.log('trade_script = ', trade_script);

            } else {
                is_trade     = 0;
                trade_script = 0;
                fields       = [];
            }

            debug.log('0343210');
            var type_site, niche_site;
            if (task_results.SITE_TYPES && task_results.SITE_TYPES.array && task_results.SITE_TYPES.array.site_types) {
                niche_site = task_results.SITE_TYPES.array.site_types.niche||'undefined';
                type_site  = task_results.SITE_TYPES.array.site_types.type||'undefined';
            } else {
                niche_site = 'undefined';
                type_site  = 'undefined';
            }


            debug.log('012342342360');

            task_results['closed_list_match'] = param.task.closed_list_match;
            data                              =
            {
                id:           param.task.task_id,
                url:          param.task.trader_url,
                type:         type_site, //  traight,gay,shemale,undefined
                niche:        niche_site, // multiniche, oneniche, undefined
                categories:   categories,
                is_trade:     is_trade, // trade form have or not 0/1
                trade_script: trade_script, // 0 or tradescript name
                fields:       fields,
                screen:     param.task.filled_form_screenshot_base64||null,
                screen_url: param.task.screen_url || null,

                debug_data: task_results
            };

            debug.log('24327567657');

        }
        else if (param.task.task_type_origin == 'test') {

            debug.log(script_name, 'prepare data for test scenario');

            data =
            {
                id:         param.task.task_id,
                screen:     param.task.filled_form_screenshot_base64||null,
                screen_url: param.task.screen_url || null,

                debug_data: task_results
            };


        } else if (param.task.task_type_origin == 'test2') {

            debug.log(script_name, 'prepare data for test2 scenario');

            data =
            {
                id:         param.task.task_id,
                screen:     param.task.filled_form_screenshot_base64||null,
                screen_url: param.task.screen_url || null,

                debug_data: task_results
            };
        }
        else if (param.task.task_type_origin == 'add') {


            debug.log(script_name, 'prepare data for add scenario');

            data =
            {
                id:              param.task.task_id,
                status:          param.task.add_status,
                reason:          param.task.fail_reason,
                TRADE_FORM_SEND: param.task.results.TRADE_FORM_SEND,

                debug_data: task_results
            };
        }
        else {
            /*
                    data =
                    {
                        code: task.code,
                        server: task.server,
                        login: task.login,
                        token: task.token,
                        balancer: task.balancer,
                        site: task.site,
                        trader: task.trader,
                        task_type: task.task_type,
                        click_task: task.click_task,
                        click_ok: task.click_ok,
                        delta: task.delta
                    };

                    if (param.hasOwnProperty('history')) {
                        data.history = param.history;
                    }*/
        }



        param.uniq_history_urls.time_consume_sec = time.time_int() - param.uniq_history_urls.ts_start;
        param.uniq_history_urls.time_consume = hhmmss(param.uniq_history_urls.time_consume_sec);
        data.time_consume_sec = param.uniq_history_urls.time_consume_sec;
        if (param.exit_case)  data.exit_case = param.exit_case;

        if (!data.debug_data) {
            data.debug_data                   = {};
            data.debug_data.uniq_history_urls = param.uniq_history_urls;
        } else {
            var debug_data = data.debug_data;
            delete data.debug_data;
            data.debug_data = debug_data;
        }

        data.debug_data.task_origin = param.task.task_origin;


    }).delay(2000).then(function () {

        //var deferred = Q.defer();

        if (param.task.filled_form_screenshot_base64) {
            debug.log('=== POST screenshot size is: ' + param.task.filled_form_screenshot_base64.length);
        }

        if (param.task.screen_url) {
            debug.log('=== POST screen_url is: ' + JSON.stringify(param.task.screen_url));
        }

        post_data = JSON.stringify(data);
        if (post_data) {
            debug.log(colors.bgWhite.black('=== POST post_data size is: ' + post_data.length));
            debug.log(colors.bgBlack.magenta('SEND FINISH DATA: '+JSON.stringify(data,'','\t')));
        }

        /*send_chank('finish_data', post_data, function () {
            console.log('send_chank cb finish');
            deferred.resolve({status: 'success'});
        });*/


        //return deferred.promise;

        return node_ipc({
            subject: 'finish_data',
            answer_required: 1,
            message: data,
        });


    }).catch(function (e) {

        debug.log(colors.bgWhite.red(script_name, ' catch: ', JSON.stringify(e)));
        deferred.resolve(e);


    }).done(function () {

        debug.log(script_name, 'done()');
        debug.log(script_name, '[POST_FINISH_TASK_SEND_SUCCESS]');

        deferred.resolve({status: 'success'});

    });

    return deferred.promise;

};


function pad(num) {
    return ("0" + num).slice(-2);
}
function hhmmss(secs) {
    var minutes = Math.floor(secs / 60);
    secs        = secs % 60;
    var hours   = Math.floor(minutes / 60)
    minutes     = minutes % 60;
    return pad(hours) + ":" + pad(minutes) + ":" + pad(secs);
}


function send_post(url, post_data, callback, retry_cnt) {
    if (!retry_cnt) retry_cnt = 0;
    retry_cnt++;


    debug.log('trying send post now ...');

    var page_task                = require('webpage').create();
    page_task.settings.userAgent = param.ua_save;

    page_task.open(url, 'post', post_data, function (status) {

        if (status !== 'success') {
            debug.log(script_name, 'Unable to send post!', status);

            if (retry_cnt > 4) {
                debug.log('stop send post retrying is to reach limit ...' + retry_cnt);
                if (callback) {
                    callback();
                    callback = null;
                }
            }
            else {
                debug.log('trying send post again after 7 sec...  (http status !== success)');
                setTimeout(function (url, post_data, callback, retry_cnt) {
                    send_post(url, post_data, function () {
                        callback();
                    }, retry_cnt);
                }, 7000, url, post_data, callback, retry_cnt);
            }


        }
        else {
            //debug.log(script_name, 'Post send success', colors.bgBlack(JSON.stringify(data)));

            //debug.log(script_name, '[POST_FINISH_TASK_SEND_SUCCESS]');

            var jsonSource = page_task.plainText || null;
            var taskObject = null;
            try {
                taskObject = JSON.parse(jsonSource);
            } catch (err) {
                console.log('err parse json ansert from finish task url jsonSource[' + jsonSource + '] status = ', JSON.stringify(status));
            }

            if (!taskObject || (taskObject && taskObject.status && taskObject.status == 'fail')) {

                if (retry_cnt > 4) {
                    debug.log('stop send post retrying is to reach limit ...' + retry_cnt);
                    if (taskObject) {
                        debug.log(script_name, '[TASK_SERVER_RESULT_JSON] [' + JSON.stringify(taskObject) + '] status = ', JSON.stringify(status));
                    }
                    if (callback) {
                        callback();
                        callback = null;
                    }
                }
                else {
                    debug.log('trying send post again after 7 sec...', JSON.stringify(jsonSource) + ' status = ', JSON.stringify(status));
                    setTimeout(function (url, post_data, callback, retry_cnt) {
                        send_post(url, post_data, function () {
                            callback();
                        }, retry_cnt);
                    }, 7000, url, post_data, callback, retry_cnt);
                }

            }
            else {
                debug.log(script_name, 'POST FINISH [TASK_SERVER_RESULT_JSON] [' + JSON.stringify(taskObject) + ']');

                if (callback) {
                    callback();
                    callback = null;
                }
            }


        }

    });

}

function send_chank(chank_name, data_str, cb_finish) {
    var data_chunks = data_str.match(/.{1,2048}/g);

    var chanks_arr = [];

    for (var c in data_chunks) {
        if (data_chunks.hasOwnProperty(c)) {
            var chank = ('<chunk_name>' + chank_name + '<chunk_name/><chunk_num>' + c + '<chunk_num/><chunk_last>' + (data_chunks.length - 1) + '<chunk_last/><chunk_data>' + data_chunks[c] + '<chunk_data/>');
            //read_chunk(chank);
            chanks_arr.push(chank);
            //console.log(chank);
        }
    }

    console.log('chanks_arr size ', chanks_arr.length);

    if (chanks_arr.length > 0) {
        write_stdout_lazy();
    } else {
        console.log('send_chank cb write_stdout_lazy 00111');
        if (cb_finish) {
            cb_finish();
            cb_finish= null;
        }
    }


    function write_stdout_lazy(callback) {

        console.log('write_stdout_lazy 001');

        var chank = chanks_arr.shift();
        console.log('write_stdout_lazy chank',chank);

        if (chanks_arr.length > 0) {
            setTimeout(function () {
                console.log('write_stdout_lazy','001');
                write_stdout_lazy()
            }, 100);

        } else {
            if (cb_finish) {
                cb_finish();
                cb_finish= null;
            }
        }

    }

}
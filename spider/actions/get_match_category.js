var config = require('./../inc/config');
var param = require('./../inc/param');
var random = require('./../lib/random');
var debug = require('./../lib/debug');
// var category_list = require('./../inc/category_list');
var Q = require('./../modules/q/q.js');

var script_name = 'get_match_category() ';


var category_list = [
    'lesbian',
    'hentai',
    'mature',
    'milf',
    'ebony',
    'threesome',
    'gay',
    'for women',
    'cartoon',
    'squirt',
    'teen',
    'anal',
    'public',
    'gangbang',
    'creampie',
    'amateurs',
    'bondage',
    'japanese',
    'big dick',
    'big tits',
    'babe',
    'reality',
    'old',
    'young',
    'orgy',
    'agranny',
    'shemale',
    'babysitter',
    'hd porn',
    'rough sex',
    'masturbation',
    'amateur',
    'interracial',
    'bbw',
    'big ass',
    'compilation',
    'double penetration',
    'massage',
    'asian',
    'pov',
    'handjob',
    'college',
    '60fps',
    'hardcore',
    'blowjob',
    'bisexual',
    'school',
    'casting',
    'parody',
    'bukkake',
    'cumshots',
    'red head',
    'russian',
    'pissing',
    'virtual reality',
    'party',
    'vintage',
    'pussy licking',
    'arab',
    'fetish',
    'indian',
    'toys',
    'german',
    'solo male',
    'pornstar',
    'funny',
    'behind the scenes',
    'webcam',
    'brazilian',
    'exclusive',
    'fisting',
    'latina',
    'small tits',
    'czech',
    'korean',
    'uniforms',
    'feet',
    'striptease',
    'italian',
    'blonde',
    'sfw',
    'british',
    'french',
    'music',
    'euro',
    'brunette',
    'smoking',
    'celebrity'
];

exports.exec = function (obj, opt) {


    var deferred = Q.defer();

    Q.fcall(function(){

        debug.log(script_name, 'start');
        //var result; //= [];
        if (typeof obj != 'undefined' && obj) {
            var result = obj.evaluate(function (opt) {

                // ///////////
                if (!opt) {
                    opt = 'all';
                } // var



                var links_arr = [];

                var links_elems = document.querySelectorAll('a');
                for (var i = 0; i < links_elems.length; i++)  {
                    if (links_elems[i]) {
                        if (links_elems[i].innerText && links_elems[i].innerText!=='') {
                            links_arr.push({
                                text: links_elems[i].innerText.toLowerCase(),
                                link: links_elems[i].href
                            })
                        }
                        else if (links_elems[i].title && links_elems[i].title!=='') {
                            links_arr.push({
                                text: links_elems[i].innerText.toLowerCase(),
                                link: links_elems[i].href
                            })
                        }
                        else if (links_elems[i].querySelectorAll('img').length > 0 &&
                            links_elems[i].querySelectorAll('img')[0].alt &&
                            links_elems[i].querySelectorAll('img')[0].alt !== ''
                        ) {
                            links_arr.push({
                                text: links_elems[i].querySelectorAll('img')[0].alt.toLowerCase(),
                                link: links_elems[i].href
                            })
                        }
                    }
                }


                //console.log(category_links_arr);
                //console.log(category_links_arr.length, category_links_uniq_arr.length);
                //console.log(category_links_uniq_arr);


                return links_arr;

                // ///////////


            }, opt);
        }
        else {
            debug.log(script_name, 'obj error');
        }

        debug.log(script_name, 'result', JSON.stringify(result.length));

        return result;



    }).delay(2000).then(function (result) {


        if ( result &&
            result.hasOwnProperty('length') &&
            result.length>0
        ) {

            var match_categories = [];

            debug.log(script_name, 'start compare result.length', JSON.stringify(result.length));
            debug.log(script_name, 'start compare category_list.length', JSON.stringify(category_list.length));


            for (var i = 0; i < result.length; i++) {
                //console.log('result',i,JSON.stringify(result[i]));
                for (var i2 = 0; i2 < category_list.length; i2++) {
                    //console.log('category_list',i2,JSON.stringify(category_list[i2]));
                    if (result[i].text && category_list[i2]) {
                        result[i].text = result[i].text.replace(/\r?\n|\r/g, '');
                        if (result[i].text.match(new RegExp(category_list[i2], ''))) {
                            if (match_categories.indexOf(category_list[i2]) == -1) {
                                console.log(script_name, 'compare match!',result[i].text,' -> ',category_list[i2]);
                                match_categories.push(category_list[i2]);
                            }
                        }
                    }
                }
            }

            //debug.log(script_name, 'end, maybe have match categorys', JSON.stringify(match_categories));
            //debug.log(script_name, 'end, maybe have match categorys length', JSON.stringify(match_categories.length));

            return match_categories;



        } else {

            debug.log(script_name, 'end, no have match category');
            return [];

        }


    }).catch(function (e) {

        debug.log(colors.bgWhite.red(script_name,' catch: ',JSON.stringify(e)));
        //deferred.reject(e);

        deferred.resolve(e);

    }).done(function(match_categories){

        debug.log(script_name, 'match_categories.length',match_categories.length);
        debug.log(script_name, 'end');

        deferred.resolve(match_categories);

    });

    return deferred.promise;


};

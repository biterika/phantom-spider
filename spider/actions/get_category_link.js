var config = require('./../inc/config');
var param = require('./../inc/param');
var random = require('./../lib/random');
var debug = require('./../lib/debug');
var Q = require('./../modules/q/q.js');


var script_name = 'get_category_link() ';

exports.exec = function (obj, opt) {
    debug.log(script_name, 'start');
    var links_all; //= [];
    if (typeof obj != 'undefined' && obj) {
        links_all = obj.evaluate(function (opt) {

            // ///////////
            if (!opt) {
                opt = 'all';
            } // var


            var links_arr = [];

            var links_elems = document.querySelectorAll('a');
            for (var i = 0; i < links_elems.length; i++) {
                if (links_elems[i]) {
                    if (links_elems[i].innerText && links_elems[i].innerText !== ' ') {
                        links_arr.push({
                            text: links_elems[i].innerText.toLowerCase(),
                            link: links_elems[i].href
                        })
                    }
                    else if (links_elems[i].title && links_elems[i].title !== ' ') {
                        links_arr.push({
                            text: links_elems[i].innerText.toLowerCase(),
                            link: links_elems[i].href
                        })
                    }
                    else if (links_elems[i].querySelectorAll('img').length > 0 &&
                        links_elems[i].querySelectorAll('img')[0].alt &&
                        links_elems[i].querySelectorAll('img')[0].alt !== ' '
                    ) {
                        links_arr.push({
                            text: links_elems[i].querySelectorAll('img')[0].alt.toLowerCase(),
                            link: links_elems[i].href
                        })
                    }
                }
            }

            var category_links_arr = [];
            for (var m = 0; m < links_arr.length; m++) {
                if (links_arr[m]) {
                    if (links_arr[m].text.match(new RegExp('categor', ''))) {
                        console.log({
                            text: links_arr[m].text,
                            link: links_arr[m].link
                        });
                        category_links_arr.push({
                            text: links_arr[m].text,
                            link: links_arr[m].link
                        })
                    }
                }
            }

            var category_links_uniq_arr = [];
            var double_count = 0;
            for (var d = 0; d < category_links_arr.length; d++) {
                if (category_links_arr[d]) {
                    double_count = 0;
                    for (var d2 = 0; d2 < category_links_uniq_arr.length; d2++) {
                        //console.log(category_links_arr[d].link, category_links_uniq_arr[d2].link)

                        if (category_links_uniq_arr[d2]) {
                            if (category_links_arr[d].link == category_links_uniq_arr[d2].link) {
                                double_count++;
                            }
                        }
                    }
                    //console.log(double_count);
                    if (double_count == 0) {
                        category_links_uniq_arr.push(category_links_arr[d2]);
                    }


                }
            }

            //console.log(category_links_arr);
            //console.log(category_links_arr.length, category_links_uniq_arr.length);
            //console.log(category_links_uniq_arr);


            return category_links_uniq_arr;

            // ///////////


        }, opt);
    } else {
        debug.log(script_name, 'obj error');
    }

    debug.log(script_name, 'end');
    return links_all;
}



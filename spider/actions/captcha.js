var system  = require('system');
var args    = system.args;

//Libs
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var colors  = require('./../modules/colors/safe.js');
var Q       = require('./../modules/q/q');
//var page    = require('webpage').create();

//Actions
//var check_popup = require('./../actions/check_popup').exec;
//var do_clicks   = require('./../actions/do_clicks').exec;
//var search_trade_link   = require('./../actions/search_trade_link').exec;
//var postload    = require('./../actions/postload').exec;

var get_captcha = require('./../actions/get_captcha').exec;
var antiCaptcha = require('./../lib/antiCaptcha');


var make_screen    = require('./../actions/make_screen').exec;
//var get_position    = require('./../actions/get_position').exec;
//var waitPageLoad        = require('./../actions/waitPageLoad').exec;
//var get_form_data        = require('./../actions/get_form_data').exec;
//var get_textContent_Page        = require('./../actions/get_textContent_Page').exec;
//var get_category_link        = require('./../actions/get_category_link').exec;
//var get_match_category        = require('./../actions/get_match_category').exec;
//var open_page        = require('./../actions/open_page').exec;
//var fill_form        = require('./../actions/fill_form').exec;
//var get_textContent_Page        = require('./../actions/get_textContent_Page').exec;
var node_ipc = require('./../actions/node_ipc').exec;

var script_name = colors.bgWhite.magenta('captcha.js');

var code;

var results = {};

exports.exec = function(page, captcha){


    var deferred = Q.defer();

    Q.fcall(function(){

        debug.log(script_name,'start','now page url ',page.url);

        return get_captcha(page, captcha.type, captcha.eval_selector);

    }).delay(2000).then(function (get_captcha_result) {

        if (!get_captcha_result) return false;


        debug.log(script_name,'get_captcha_result = ',JSON.stringify(get_captcha_result));

        page.clipRect = get_captcha_result.rect; // { top: 0, left: 0, width: 1280, height: 1280 };
        param.task.captcha_image_base64 = page.renderBase64('JPEG');
        make_screen(page);
        page.clipRect = { top: 0, left: 0, width: 1280, height: 1280 };
        page.viewportSize = {
            width:  param.task.sx || 1366,
            height: param.task.sy || 768
        };

        return node_ipc({
            subject: 'captcha',
            answer_required: 1,
            message: {
                type: captcha.type,
                innerText: get_captcha_result.innerText,
                image: param.task.captcha_image_base64,
            },
        });


    }).then(function (get_captcha_result) {

        var deferred = Q.defer();

        if (captcha.type == 'image') {

            console.log('captcha.type == image call antiCaptcha');
            antiCaptcha.request(param.task.captcha_image_base64, captcha, function (res) {

                console.log('antiCaptcha mod answer',JSON.stringify(res));

                // antiCaptcha mod answer {"status":"success","answer":"MUUKC"}
                if (res &&  res.answer) {
                    console.log('deferred.resolve antiCaptcha mod answer',JSON.stringify(res));
                    deferred.resolve(res);
                } else {
                    deferred.resolve(get_captcha_result);
                }

            });

            // deferred.resolve(get_captcha_result);

        } else {
            deferred.resolve(get_captcha_result);
        }

        return deferred.promise;

    }).catch(function (e) {

        debug.log(colors.bgWhite.red(script_name,' catch: ',JSON.stringify(e)));
        deferred.reject(e);


    }).done(function(result){

        debug.log(script_name, 'done(function()');
        debug.log(script_name, 'done(function() result ',JSON.stringify(result));

        var answer = 'unknown';

        if (result && result.answer) {

            debug.log(script_name, 'done(function() answer');
            answer = result.answer;
        }
        debug.log(script_name,'node_ipc_result = >> ',JSON.stringify(answer));

        deferred.resolve(answer);

    });


    return deferred.promise;

};

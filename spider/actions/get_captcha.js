var config  = require('./../inc/config');
var param   = require('./../inc/param');
var random  = require('./../lib/random');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var script_name = 'get_captcha() ';

exports.exec = function(obj,type,eval_selector){

    var deferred = Q.defer();

    debug.log(script_name, 'start');

    if(typeof obj != 'undefined' && obj){

      obj.includeJs('http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js', function() {
            var result = obj.evaluate(function (type, eval_selector) {

                // ///////////
                console.log('get_captcha(), start,  eval_selector = ', eval_selector);

                var element = null;

                try {
                    element = eval(eval_selector);
                } catch (err) {
                    console.log('catch err eval' + JSON.stringify(err));
                }

                if (element) {
                    console.log('get_captcha(), ok select element = ', element);
                    var rect      = element.getBoundingClientRect();
                    var innerText = element.innerText;
                    var innerHTML = element.innerHTML;

                    return {
                        status:    'success',
                        rect:      rect,
                        innerText: innerText,
                        innerHTML: innerHTML,
                    }
                }

                console.log('get_captcha(), error select elemnt by eval_selector ' + eval_selector);

                return {status: 'fail'};

                // ///////////


            }, type, eval_selector);
            console.log(result);
            deferred.resolve(result);
      });

    } else {
        debug.log(script_name, 'obj error');
        deferred.resolve(false);
    }

    //debug.log(script_name, 'end');
    //return result;

    return deferred.promise;
};

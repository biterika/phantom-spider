var system = require('system');
//var args   = system.args;

//Libs
//var config = require('./../inc/config');
//var param  = require('./../inc/param');
//var debug  = require('./../lib/debug');
//var colors = require('./../modules/colors/safe.js');
var Q      = require('./../modules/q/q');
//var page    = require('webpage').create();

//Actions
//var check_popup = require('./../actions/check_popup').exec;
//var do_clicks   = require('./../actions/do_clicks').exec;
//var search_trade_link   = require('./../actions/search_trade_link').exec;
//var postload    = require('./../actions/postload').exec;

//var get_captcha = require('./../actions/get_captcha').exec;


//var make_screen = require('./../actions/make_screen').exec;
//var get_position    = require('./../actions/get_position').exec;
//var waitPageLoad        = require('./../actions/waitPageLoad').exec;
//var get_form_data        = require('./../actions/get_form_data').exec;
//var get_textContent_Page        = require('./../actions/get_textContent_Page').exec;
//var get_category_link        = require('./../actions/get_category_link').exec;
//var get_match_category        = require('./../actions/get_match_category').exec;
//var open_page        = require('./../actions/open_page').exec;
//var fill_form            = require('./../actions/fill_form').exec;
//var get_textContent_Page = require('./../actions/get_textContent_Page').exec;

var script_name = colors.bgWhite.magenta('node_ipc.js');


exports.exec = function (msg) {

    var deferred = Q.defer();

    var subject         = msg.subject|| null;
    var answer_required = msg.answer_required;
    var message         = JSON.stringify(msg.message);
    var finished = false;

    debug.log(script_name, 'start', ' subject = ', subject, ' answer_required = ', answer_required, ' message.len = ', message.length);

    send_chank(subject,answer_required, message,
        function send_done() {

        if (answer_required == 1) {
            debug.log(script_name, 'answer_required, [WAIT_ANSWER_AT_STDIN]');
            var input = system.stdin.readLine();
            if (input) {
                console.log('phantom js recived stdin input = ', JSON.stringify(input));

                var result_obj = null;
                try {
                    result_obj = JSON.parse(input);
                } catch(err) {
                    console.log('error json parse node_ipc_result')
                }

                if (!finished) {
                    finished = true;
                    deferred.resolve(result_obj);
                }
            }
        } else {

            if (!finished) {
                finished = true;
                deferred.resolve({status: 'success'});
            }
        }

    });



    setTimeout(function () {
        if (!finished) {
            finished = true;
            deferred.resolve({status: 'fail'});
        }
    }, 20 * 1000);


    return deferred.promise;
};



function send_chank(subject, answer_required, data_str, cb_finish) {
    var data_chunks = data_str.match(/.{1,2048}/g);

    var chanks_arr = [];

    for (var c in data_chunks) {
        if (data_chunks.hasOwnProperty(c)) {
            var chank = ('<call_agent_subject>' + subject + '<call_agent_subject/><call_agent_answer_required>' + answer_required + '<call_agent_answer_required/><chunk_num>' + c + '<chunk_num/><chunk_last>' + (data_chunks.length - 1) + '<chunk_last/><chunk_data>' + data_chunks[c] + '<chunk_data/>');
            chanks_arr.push(chank);
        }
    }

    console.log('chanks_arr size ', chanks_arr.length);

    if (chanks_arr.length > 0) {
        write_stdout_lazy();
    } else {
        //console.log('send_chank cb write_stdout_lazy 00111');
        if (cb_finish) {
            cb_finish();
            cb_finish= null;
        }
    }


    function write_stdout_lazy() {

        if (chanks_arr.length > 0) {
            var chank = chanks_arr.shift();
            system.stdout.writeLine(chank);
        } else {
            if (cb_finish) {
                cb_finish();
                cb_finish= null;
            }
        }

        if (chanks_arr.length > 0) {
            setTimeout(function () {
                //console.log('write_stdout_lazy','001');
                write_stdout_lazy()
            }, 100);

        } else {
            if (cb_finish) {
                cb_finish();
                cb_finish= null;
            }
        }

    }

}
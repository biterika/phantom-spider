var param   = require('./../inc/param');
var domain  = require('./../lib/domain');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');
var colors  = require('./../modules/colors/safe.js');

var script_name = 'get_task_from_json()';

var result = {};
var taskObject = {};

exports.exec = function(task_json, obj){

    var deferred = Q.defer();

    Q.fcall(function(){


                         debug.log(script_name, '#### GET TASK FROM JSON ', task_json);
                         debug.log(' '); debug.log(task_json);
                        taskObject = JSON.parse(task_json);

                        param.task.jsonSource = task_json;
                        param.task.task_origin = taskObject;

                        debug.log(script_name,'\n'+colors.bgMagenta.white('[TASK_JSON] ' )+JSON.stringify(taskObject,'','\t')+'\n');

                        // {"status":"no_task"}
                        if(taskObject.hasOwnProperty('status')) {
                            if(taskObject.status == 'no_task') {
                                debug.log(script_name,'[NO_TASK] ['+task_json+']');
                                throw new Error('NO_TASK');
                        }}

                        if(taskObject.hasOwnProperty('message')) {
                            if(taskObject.message == 'No task') {
                                debug.log(script_name,'[NO_TASK] ['+task_json+']');
                                throw new Error('NO_TASK');

                        }}
                        if(taskObject.hasOwnProperty('message')) {
                            if(taskObject.message == 'fail') {
                                debug.log(script_name,'[TASK_SERVER_ERROR] ['+task_json+']');
                                throw new Error('NO_TASK');
                        }}

                       // // debug.log(script_name, ,'jsonSource = ', jsonSource);
                        //// debug.log(script_name, 'taskObject = ', JSON.stringify(taskObject));

                        return taskObject;

    }).then(function (taskObject) {

                        // проверка наличия обязательных параметров задачи


                        // debug.log(script_name,'next...');

                        if (taskObject &&
                            taskObject.hasOwnProperty('task_type') &&
                            taskObject.hasOwnProperty('id') &&
                            taskObject.hasOwnProperty('url')
                        ){

                            debug.log(script_name,'[TASK_TYPE] ['+taskObject.task_type+']');

                            // debug.log(script_name,'next2...');

                            if (taskObject.id) {
                                debug.log(script_name,'[TASK_ID] ['+JSON.stringify(taskObject.id)+']');
                                param.task.task_id = taskObject.id;

                            } else {
                                // debug.log(script_name,'next22...');
                                // debug.log(script_name,'ERROR id param have incorrect value', taskObject.id);
                                result = {status: 'fail'};
                                deferred.resolve(result);
                            }

                            // debug.log(script_name,'next23...');
                            // debug.log(script_name,'next233...');
                            // debug.log(script_name,'next2333...');
                            //// debug.log(script_name,'next3...',taskObject.url);

                            if (taskObject.url) {

                                debug.log(script_name,'[TASK_SITE_URL] ['+JSON.stringify(taskObject.url)+']');
                                // debug.log(script_name,'next30...',taskObject.url);
                                taskObject.url = 'http://'+taskObject.url;

                                // debug.log(script_name,'next31...',taskObject.url);

                                param.task.trader = domain.getDomain(taskObject.url);
                                // debug.log(script_name,'next32...',param.task.trader);
                                param.task.trader_url = taskObject.url;
                                // debug.log(script_name,'next34...',param.task.trader_url);

                                if (!param.task.trader) {
                                    // debug.log(script_name, 'ERROR url (2) param have incorrect value', taskObject.url);
                                    result = {status: 'fail'};
                                    deferred.resolve(result);
                                }
                            } else {

                                // debug.log(script_name,'next3else...');
                                // debug.log(script_name, 'ERROR url (1) param have incorrect value', taskObject.url);
                                result = {status: 'fail'};
                                deferred.resolve(result);
                            }


                            // debug.log(script_name,'next4...');
                            // тип задачи

                            if (taskObject.task_type == 'click') {

                                param.task.disable_img_js = true;
                                obj.settings.loadImages = false;

                                // id, task_type, url, count
                                if (taskObject.count) {
                                    param.task.click_task = parseInt(taskObject.count);
                                } else {
                                    // debug.log(script_name, 'ERROR count param have incorrect value', taskObject.count);
                                    param.task.click_task = 50;
                                    result = {status: 'fail'};
                                    deferred.resolve(result);
                                }

                                param.task.task_type = 'trade_add.click';
                                param.task.task_type_origin = taskObject.task_type;

                            }
                            else if (taskObject.task_type == 'discover') {
                                // id, task_type, url
                                param.task.task_type = 'trade_add.discover.js';
                                param.task.task_type_origin = taskObject.task_type;

                            }
                            else if (
                                taskObject.task_type == 'test' ||
                                taskObject.task_type == 'test2' ||
                                taskObject.task_type == 'add' ) {


                                param.task.task_type = 'trade_add.discover.js';
                                param.task.task_type_origin = taskObject.task_type;

                                param.task.task_sub_type = taskObject.task_type;
                                param.task.task_type_origin = taskObject.task_type;

                                // id, task_type, url, proxy, fields[]

                                // debug.log(script_name,'next6...');
                               /* if (taskObject.proxy) {
                                    param.proxy = taskObject.proxy;
                                } else {
                                    // debug.log(script_name, 'ERROR proxy param have incorrect value', taskObject.proxy);
                                    result = {status: 'fail'};
                                    deferred.resolve(result);
                                }*/

                                // debug.log(script_name,'next7...');
                                if (taskObject.fields) {
                                    param.task.fields = taskObject.fields;
                                } else {
                                    // debug.log(script_name, 'ERROR fields param have incorrect value', taskObject.fields);
                                    result = {status: 'fail'};
                                    deferred.resolve(result);
                                }

                                if (taskObject.success_message) {
                                    param.task.success_message = taskObject.success_message;
                                    debug.log(script_name,' param.task.success_message = '+JSON.stringify(taskObject.success_message));
                                } else {
                                    debug.log(script_name,' param.task.success_message = null');
                                }

                                // debug.log(script_name,'next8...');

/*
                                if (param.proxy == 'none') {
                                    // debug.log('');
                                    // debug.log(script_name,  colors.red.bgWhite.bold(' ----==== ATTENTION! PROXY NOT SET !!!  ====----, param proxy = ', param.proxy));
                                    // debug.log('');
                                } else {
                                    // debug.log('');
                                    // debug.log(script_name,  colors.red.bgWhite.bold('SET PROXY', param.proxy));
                                    var parse_proxy = param.proxy.split(':');
                                    // debug.log(script_name,  colors.red.bgWhite.bold('SET PROXY  host:', parse_proxy[0], ' port: ', parse_proxy[1], ' type: socks5'));

                                    //
                                    // phantom.setProxy(parse_proxy[0], parse_proxy[1], 'manual', '', '');
                                    // https://github.com/ariya/phantomjs/pull/11829/commits/84c31822a2e5eba21fe42298ec27ec4ccab95667
                                    //
                                    phantom.setProxy(parse_proxy[0], parse_proxy[1], 'socks5', '', '');

                                    // debug.log(script_name,  colors.red.bgWhite.bold('SET PROXY  - finished'));
                                    // debug.log('');

                                }*/

                            }
                            else {

                                // debug.log(script_name,'next8...');
                                // debug.log(script_name, 'ERROR task_type param have incorrect value', taskObject.task_type);
                                result = {status: 'fail'};
                                deferred.resolve(result);
                            }

                            // debug.log(script_name,'next11...');

                        }
                        else {
                            // debug.log(script_name, 'ERROR get task from json');

                            result = {status: 'fail'};
                            deferred.resolve(result);
                        }

        // debug.log(script_name,'next131...');

                        return true;

    }).then(function () {
                        param.task.site = 'localhost';
                        //param.task.param = domain.getDomain(getQueryVariable(requestData.url,'param'));
                        param.task.lang = 'en-us,en;q=0.5'.split(',')[0];
                        param.task.useragent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2671.0 Safari/537.36';

                        //useragent = param.task.useragent;
                        return true;
    })
        /*.catch(function (e) {

        debug.log(script_name, colors.bgWhite.red(script_name,'catch!'));
        // debug.log(script_name, colors.bgWhite.red(script_name,'catch!',JSON.stringify(e)));

        console.log('catch e',e.message)
/!*

        debug.error(script_name, 'ERROR:', JSON.stringify(e));

        if(e.message){
            debug.error(script_name, 'ERROR:', e.message, config.errors[e.message]);
            debug.error(script_name, 'ERROR:', e.stack);
        }
*!/


        debug.log(script_name, colors.bgWhite.red(script_name,'catch!'));
        result = {status: e.message};
        deferred.reject(result);


    })*/
        .done(function(){

        debug.log(script_name, 'param.task: ', JSON.stringify(param.task));
        result = {status: 'success'};
        deferred.resolve(result);
    });

    return deferred.promise;

};

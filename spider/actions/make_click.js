var config = require('./../inc/config');
var param  = require('./../inc/param');
var random = require('./../lib/random');
var debug  = require('./../lib/debug');
var domain = require('./../lib/domain');
var Q      = require('./../modules/q/q.js');

var colors       = require('./../modules/colors/safe.js');
var random_thumb = require('./random_thumb').exec;
var click_xy     = require('./click_xy').exec;
var check_pages  = require('./check_pages').exec;
//var make_screen = require('./../actions/make_screen2file').exec;
var addToHistory = require('./addToHistory').exec;


exports.exec = function (obj) {
    debug.log('');
    debug.log('make_click()  ', 'obj.url=', obj.url, 'obj.id=', obj.id);

    var obj_main      = false;
    var obj_for_click = false;

    var page_id_for_click;

    if (param.task.disable_img_js) {
        config.timeout.before_click = 2000;
    }


    debug.log('make_click()  ', 'wait timeout before click, timeout sec:', config.timeout.before_click / 1000);

    var deferred = Q.defer();


    //wait timeout before click
    setTimeout(function () {
        debug.log('make_click()  ', 'wait timeout before click -  finish');

        if (!obj_main) {
            obj_main     = obj;
            obj.pages[0] = obj;
        }
        if (!obj_for_click) {
            obj_for_click = obj_main;
        }

        if (config.const.debug) {
            //make_screen(obj_for_click);
        }

        var thumb;
        var max_id = 0;

        var pages_count    = 0;
        var page_id_first;
        var pages_id_arr   = [];
        var pages_urls_str = '\n';

        debug.log('make_click()  for loop - start');
        for (var id in obj.pages) {

            debug.log('make_click()                     for loop chk', ' pages_count= ', pages_count, 'id:', id);
            if (obj.pages.hasOwnProperty(id)) {
                if (obj.pages[id].hasOwnProperty('id')) {

                    var loop_page_id = obj.pages[id].id;

                    pages_count++;

                    debug.log('make_click()  for loop ', ' pages_count= ', pages_count, 'id:', id, ' page_id: ', obj.pages[id].id, 'page_domain: ', obj.pages[id].domain, 'page_url: ', obj.pages[id].url);

                    page_id_first = id;

                    pages_id_arr.push(obj.pages[id].id);

                    pages_urls_str += '\t\t' + pages_count + '\tpage_id: ' + obj.pages[id].id + '\t' + obj.pages[id].url + '\n';

                    debug.log('mc004');
                    // проверяем есть ли тумбы ////////
                    thumb = random_thumb(obj.pages[id]);
                    debug.log('make_click() ', ' @ ', colors.bgBlack.white('checking tumbs = ', JSON.stringify(thumb)));

                    debug.log('mc005');
                    if (!thumb.hasOwnProperty('x')) {
                        debug.log(colors.bgCyan.red('no have tumbs page_id: ', obj.pages[id].id, 'url: ', obj.pages[id].url,'l:85'));
                        obj.pages[id].no_have_thumb = true;

                        if (obj.pages[id].id && obj.pages[id].url) {
                            debug.log('make_click()', 'add page to array for pages_for_close, id:', obj.pages[id].id, 'page_id: ', obj.pages[id].id, obj.pages[id].url);
                            param.pages_for_close.push(obj.pages[id].id);
                        }
                    } else {

                        for (var i in param.pages_for_close) {
                            if (param.pages_for_close.hasOwnProperty(i) && param.pages_for_close[i]) {
                                if (param.pages_for_close[i] == obj.pages[id].id) {
                                    delete param.pages_for_close[i];
                                }
                            }
                        }

                        debug.log(colors.bgCyan.red('have tumbs page_id: ', obj.pages[id].id, 'url: ', obj.pages[id].url,'l:102'));
                        addToHistory({url: obj.pages[id].url, siteType: 'thumb', page: obj.pages[id].id}, obj);
                        debug.log('00001');
                    }
                    /////////


                    debug.log('mc006');
                } else {
                    pages_urls_str += '\t\t' + pages_count + '\tpage_id: ' + 'n/a' + '\t' + obj.pages[id].url + '\n';
                }
            }
        }
        debug.log('make_click()  for loop - end, pages_count=', pages_count, '    page_id_first ', page_id_first);

        if (pages_count == 0) {
            debug.log('pages_count==0 case start');

            /*debug.log('pages_count==0 case obj_main', 'page_id: ',obj_main.id, ' page_url: ',obj_main.url);


            if (!obj.hasOwnProperty('pages')) {
                debug.log('pages_count==0 case !obj.hasOwnProperty pages');
                obj.pages=[];
                debug.log('pages_count==0 case obj.pages=[];');
            } else {

                debug.log('pages_count==0 case obj hasOwnProperty pages');
            }

            debug.log('pages_count==0 case obj.pages[0]=obj_main');
            obj.pages[0]=obj_main;

            debug.log('pages_count==0 case obj.pages[0]=obj_main');
            //debug.log('pages_count==0 case obj.pages[0]', 'page_id: ',obj.pages[0].id, ' page_url: ',obj.pages[0].url);



            deferred.resolve({
                status: 'success'
            });
            return deferred.promise;*/
        }

        if (param.hasOwnProperty('history')) {
            if (param.history.hasOwnProperty('domains')) {
                var thumb_domains_count = param.history.domains.length;
            }
        }

        console.log(colors.bgWhite.blue('@@@@@', '   open pages: ', pages_count, ' of ', param.max_open_pages, '    make clicks: ', param.task.click_ok, ' of ', param.task.click_task, '    thumb_domains_count: ', thumb_domains_count, '\n', pages_urls_str));

        /*if (pages_count>param.max_open_pages && obj.pages.hasOwnProperty(page_id_first)) {
            if (obj.pages[page_id_first].id) {
                debug.log('make_click()', '(2) add page to array for close(), page_id: ', obj.pages[page_id_first].id, obj.pages[page_id_first].url);
                param.pages_for_close.push(obj.pages[page_id_first].id);
            }
        }*/


        debug.log('mc008');
        var pages_arr_count = 0;
        pages_id_arr        = [];
        for (var id in obj.pages) {
            if (obj.pages.hasOwnProperty(id)) {
                if (obj.pages[id].hasOwnProperty('id')) {
                    pages_id_arr[pages_arr_count] = obj.pages[id].id;
                    pages_arr_count++;
                }
            }
        }


        //debug.log('generate rand_page_count_id from 0 to '+pages_arr_count);
        var rand1 = random.int(0, pages_arr_count);
        debug.log('generate rand_page_count_id from 0 to ' + pages_arr_count, ' = ', rand1);
        var rand_page_count_id = pages_id_arr[rand1];
        console.log('rand_page_count_id = ', rand_page_count_id);

        for (var id3 in obj.pages) {
            if (obj.pages.hasOwnProperty(id3)) {
                if (obj.pages[id3].hasOwnProperty('id')) {
                    if (obj.pages[id3].id == rand_page_count_id) {

                        debug.log(colors.bgGreen('make_click() 1', 'change obj_for_click \n',
                            '           before    page_id: ', obj_for_click.id, 'url: ', obj_for_click.url + '\n',
                            '           change_to page_id: ', obj.pages[id3].id, 'url: ', obj.pages[id3].url + '\n'
                        ));

                        obj_for_click = obj.pages[id3];
                    }
                }
            }
        }

        debug.log('mc018');

        //console.log('obj_for_click',obj_for_click.url,obj_for_click.content.length);

        //var filename = '/usr/share/biterika/screen/'+Date.now()+'-'+obj.id+'-'+random.char(4)+'.jpg';
        //debug.log('render '+filename);
        //obj.render(filename, {format: 'jpeg', quality: '10'});

        //is a thumbs have in page?
        debug.log(colors.bgMagenta.white('make_click()  2', 'CHECK THUMBS...', 'page_id: ', obj_for_click.id, ' url= ', obj_for_click.url));
        debug.log('mc019');
        thumb = random_thumb(obj_for_click);
        debug.log('mc020');
        debug.log('make_click() ', ' @ ', 'random thumb selected = ', JSON.stringify(thumb));

        debug.log('mc021');
        if (!thumb.x || !thumb.y) {
            debug.log('mc022');
            /*setTimeout(function(){
                make_screen(obj_for_click);
            }, 200);*/

            debug.log('make_click()  !thumb.x || !thumb.y', 'obj_for_click.url', obj_for_click.url);
            //debug.log('content',obj_for_click.content);

            obj_for_click = obj_main;
            debug.log('mc023');
            deferred.resolve({
                status: 'error'
            });

        } else {
            debug.log('mc024');

            debug.log(colors.bgCyan.red('make_click()  ', ' tumbs have ', 'obj_for_click.url', obj_for_click.url));
            addToHistory({url: obj_for_click.url, siteType: 'thumb', page: obj_for_click.id}, obj);
            debug.log('mc025');

            param.have_thumb++;

            debug.log('mc026');

            var x = random.int(thumb.x + 5, thumb.x + thumb.width - 5);
            var y = random.int(thumb.y + 5, thumb.y + thumb.height - 5);

            debug.log('mc027');

            param.popupLoading = false;
            param.page_created = false;


            /*            // проверяем есть ли тумбы ////////
                        thumb = random_thumb(obj_for_click);
                        debug.log('make_click() ', ' @ ', colors.bgBlack.white('checking tumbs = ', JSON.stringify(thumb)));

                        debug.log('mc005');
                        if (!thumb.hasOwnProperty('x')) {
                            debug.log(colors.bgCyan.gray('no have tumbs page_id: ', obj_for_click.id, 'url: ', obj_for_click.url));


                            debug.log('make_click()', 'add page to array for close(), id:',id,'page_id: ',obj_for_click.id,obj_for_click.url);
                            param.pages_for_close.push(obj_for_click.id);
                        } else {
                            debug.log(colors.bgCyan.red('have tumbs page_id: ', obj_for_click.id, 'url: ', obj_for_click.url));
                            addToHistory({url: obj_for_click.url, siteType: 'thumb', page: obj_for_click.id});
                        }
                        /////////*/
            var rand_click_type = random.int(0, 1);
            if (pages_count > param.max_open_pages) { //if (rand_click_type == 1) {
                // close any page but not obj_for_click


                for (var p in obj.pages) {
                    if (obj.pages.hasOwnProperty(p) && obj.pages[p].id && obj_for_click.id !== obj.pages[p].id) {
                        param.pages_for_close.push(obj.pages[p].id);
                        console.log(colors.red('pages_count > param.max_open_pages -> pages_for_close ' + obj.pages[p].id + ' '))
                        break;
                    }
                }

                // закрываем страницы с одинаковым доменом
                var page_domains = [];
                for (var p in obj.pages) {
                    if (obj.pages.hasOwnProperty(p) && obj.pages[p].id && obj.pages[p].url) {

                        var page_domain = domain.getDomain(obj.pages[p].url);

                        if (page_domain.indexOf(page_domains) >=0 &&
                            obj_for_click.id !== obj.pages[p].id) {
                            param.pages_for_close.push(obj.pages[p].id);
                            console.log(colors.red('dublicate domains -> pages_for_close page_domains' + JSON.stringify(page_domains) + ' page_id: ' + obj.pages[p].id + ' page_domain ' + page_domain + ' url ' + obj.pages[p].url))
                        }
                        page_domains.push(page_domain);
                    }
                }

            }
            if (pages_count > param.max_open_pages) { //if (rand_click_type == 1) {
                debug.log('make_click()  ', 'click type 1 left');
                debug.log('make_click()  ', 'start click_xy left');
                click_xy(obj_for_click, x, y, 'left');
                debug.log('make_click()  ', 'finish click_xy left');
                param.task.click_ok++;
            }
            else {

                debug.log(colors.bgYellow.black('make_click() ', ' evalute() OPEN PAGE IN NEW WINDOW - start', 'from page_id: ', obj_for_click.id, 'click on the page :', obj_for_click.url));

                obj_for_click.evaluate(function (x, y) {

                    function rand(min, max) {
                        return Math.floor(Math.random() * (max - min + 1)) + min;
                    }

                    function getOffsetRect(elem) {
                        var box        = elem.getBoundingClientRect();
                        var body       = document.body;
                        var docElem    = document.documentElement;
                        var scrollTop  = window.pageYOffset || docElem.scrollTop || body.scrollTop;
                        var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
                        var clientTop  = docElem.clientTop || body.clientTop || 0;
                        var clientLeft = docElem.clientLeft || body.clientLeft || 0;
                        var top        = box.top + scrollTop - clientTop;
                        var left       = box.left + scrollLeft - clientLeft;
                        var width      = elem.offsetWidth;
                        var height     = elem.offsetHeight;
                        return {el: elem, x: Math.round(left), y: Math.round(top), width: width, height: height}
                    }

                    var thumb_top_res = [];
                    var mass          = document.querySelectorAll("a img");
                    var sizes         = {};

                    for (var i = 0; i < mass.length; i++) {
                        if (mass[i].height >= 50) {
                            var rect = getOffsetRect(mass[i]);
                            var size = rect.width + 'x' + rect.height;
                            if (typeof sizes[size] == 'undefined') sizes[size] = [];
                            sizes[size].push(rect);
                        }
                    }

                    var len = 0;
                    for (var size in sizes) {
                        if (len < sizes[size].length) {
                            len           = sizes[size].length;
                            thumb_top_res = sizes[size].slice(0, 10);
                        }
                    }

                    var ev = document.createEvent("MouseEvent");
                    var el = thumb_top_res[rand(0, thumb_top_res.length - 1)].el.parentNode;
                    el.setAttribute('target', '_blank');
                    ev.initMouseEvent(
                        "click",
                        true /* bubble */, true /* cancelable */,
                        window, null,
                        0, 0, 0, 0, /* coordinates */
                        false, false, false, false, /* modifier keys */
                        0 /*left*/, null
                    );

                    el.dispatchEvent(ev);

                }, x, y);

                param.task.click_ok++;

                debug.log(colors.bgYellow.black('make_click() ', ' evalute() OPEN PAGE IN NEW WINDOW - finished'));

            }

            debug.log('hyi 002');


            if (param.task.disable_img_js) {
                config.timeout.wait_popup = 1500;
            }

            debug.log('wait timeout before open popups timeout', config.timeout.wait_popup / 1000);
            setTimeout(function () {

                //var random_run = random.int(0,1);
                //if (random_run) {
                    check_pages(obj_main);
                //}

                deferred.resolve({
                    status: 'success'
                });
            }, config.timeout.wait_popup);

            //wait timeout before open popups
            /*setTimeout(function(){
                debug.log('mc030');
                //wait when popup loading will be finished
                waitFor(function(){
                    debug.log('mc032');
                    return !param.popupLoading;
                }, function(){
                    debug.log('mc033');
                    obj_for_click = check_pages(obj_main);
                    debug.log('mc034');
                    deferred.resolve({
                        status: 'success'
                    });
                    debug.log('mc035');
                }, config.timeout.loading_popup);

                debug.log('mc036');
            }, config.timeout.wait_popup);*/

            debug.log('mc037');
        }
        debug.log('mc038');
    }, config.timeout.before_click);

    //debug.log('make_click()','deferred.promise');

    return deferred.promise;

};


//wait for something
function waitFor(testFx, onReady, timeOut, timeFrequency) {
    var maxtimeOutMillis = timeOut ? timeOut : 3000,
        start            = new Date().getTime(),
        condition        = false,
        interval         = setInterval(function () {
            if ((new Date().getTime() - start < maxtimeOutMillis) && !condition) {
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx());
            } else {
                if (!condition) {
                    //console.log("'waitFor()' timeout");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                } else {
                    //console.log("'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                }
            }
        }, timeFrequency);
}

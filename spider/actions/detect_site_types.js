var config = require('./../inc/config');
var param  = require('./../inc/param');
//var random = require('./../lib/random');
var debug  = require('./../lib/debug');
// var category_list = require('./../inc/category_list');
var Q      = require('./../modules/q/q.js');

var script_name = 'detect_site_types() ';


var cat_types = {

    straight: [
        'big tits',
        'hardcore',
        'japanese',
        'asian',
    ],

    gay: [
        'gay',
    ],

    shemale: [
        'shemale',
    ]
};

var category_list = [
    'lesbian',
    'hentai',
    'mature',
    'milf',
    'ebony',
    'threesome',
    'for women',
    'cartoon',
    'squirt',
    'anal',
    'public',
    'gangbang',
    'creampie',
    'amateurs',
    'bondage',
    'big dick',
    'babe',
    'reality',
    'old',
    'young',
    'orgy',
    'agranny',
    'babysitter',
    'hd porn',
    'rough sex',
    'masturbation',
    'amateur',
    'interracial',
    'bbw',
    'big ass',
    'compilation',
    'double penetration',
    'massage',
    'pov',
    'handjob',
    'college',
    '60fps',
    'hardcore',
    'blowjob',
    'bisexual',
    'school',
    'casting',
    'parody',
    'bukkake',
    'cumshots',
    'red head',
    'russian',
    'pissing',
    'virtual reality',
    'party',
    'vintage',
    'pussy licking',
    'arab',
    'fetish',
    'indian',
    'toys',
    'german',
    'solo male',
    'pornstar',
    'funny',
    'behind the scenes',
    'webcam',
    'brazilian',
    'exclusive',
    'fisting',
    'latina',
    'small tits',
    'czech',
    'korean',
    'uniforms',
    'feet',
    'striptease',
    'italian',
    'blonde',
    'sfw',
    'british',
    'french',
    'music',
    'euro',
    'brunette',
    'smoking',
    'celebrity'
];

exports.exec = function (site_cats) {


    var deferred = Q.defer();

    Q.fcall(function () {

        debug.log(script_name, 'start');

        var score_type = {
            straight: 0,
            gay:      0,
            shemale:  0,
        };

        var site_types = {
            type:  'undefined',  // type: 'undefined', //  straight,gay,shemale,undefined
            niche: 'undefined'  // niche: 'undefined', // multiniche, oneniche, undefined
        };

        var site_cats_amount = site_cats.length;

        for (var i in cat_types) {
            for (var i2 in cat_types[i]) {
                for (var i3 in site_cats) {
                    if (site_cats[i3] == cat_types[i][i2]) {
                        score_type[i]++;
                    }
                }
            }
        }

        debug.log(script_name, ' ');
        debug.log(script_name, 'score_type: ', JSON.stringify(score_type));
        debug.log(script_name, ' ');

        //site_cats_amount


        if (param.task.trader_url.match(new RegExp('gay', ''))) {
            site_types = {
                type:  'gay',
                niche: 'oneniche'
            };
        } else if (param.task.trader_url.match(new RegExp('shemale', ''))) {
            site_types = {
                type:  'shemale',
                niche: 'oneniche'
            };
        } else if (param.task.trader_url.match(new RegExp('teen', ''))) {
            site_types = {
                type:  'straight',
                niche: 'oneniche'
            };
        } else {

            if (score_type.gay > score_type.straight &&
                score_type.gay > score_type.shemale) {
                site_types = {
                    type:  'gay',
                    niche: 'undefined'
                };
            } else if (
                score_type.straight > score_type.gay &&
                score_type.straight > score_type.shemale) {
                site_types = {
                    type:  'straight',
                    niche: 'undefined'
                };
            } else if (
                score_type.shemale > score_type.straight &&
                score_type.shemale > score_type.gay) {
                site_types = {
                    type:  'shemale',
                    niche: 'undefined'
                };
            }
            else {
                site_types = {
                    type:  'undefined',
                    niche: 'undefined'
                };
            }
        }


        return {score_type: score_type, site_types: site_types};


    }).catch(function (e) {

        debug.log(colors.bgWhite.red(script_name, ' catch: ', JSON.stringify(e)));
        //deferred.reject(e);

        deferred.resolve(e);

    }).done(function (score_type_result) {

        debug.log(script_name, 'score_type_result', JSON.stringify(score_type_result));
        debug.log(script_name, 'end');

        deferred.resolve(score_type_result);

    });

    return deferred.promise;


};

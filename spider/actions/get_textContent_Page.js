var config  = require('./../inc/config');
var param   = require('./../inc/param');
var random  = require('./../lib/random');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var script_name = 'get_textContentPage() ';

exports.exec = function(obj,opt){
    debug.log(script_name, 'start');

    if(typeof obj != 'undefined' && obj){
        var result = obj.evaluate(function(opt) {

            // ///////////
            if (!opt) {opt='all';}


            return document.body.innerText;

            // ///////////


        },opt);
    } else {
        debug.log(script_name, 'obj error');
    }

    debug.log(script_name, 'end');
    return result;



};

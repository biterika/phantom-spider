var config = require('./../inc/config');
var param  = require('./../inc/param');
var random = require('./../lib/random');
var debug  = require('./../lib/debug');
var domain = require('./../lib/domain');
var Q      = require('./../modules/q/q.js');

var colors       = require('./../modules/colors/safe.js');
var random_thumb = require('./random_thumb').exec;
var click_xy     = require('./click_xy').exec;
var check_pages  = require('./check_pages').exec;
//var make_screen = require('./../actions/make_screen2file').exec;
var addToHistory = require('./addToHistory').exec;
var script_name = '<make_click2.js>';

exports.exec = function (obj) {

    var deferred = Q.defer();

    var obj_main      = false;
    var obj_for_click = false;
    if (!obj_main) {
        obj_main     = obj;
        obj.pages[0] = obj;
    }
    if (!obj_for_click) {
        obj_for_click = obj_main;
    }


    var thumb;
    var max_id = 0;

    var pages_count    = 0;
    var page_id_first;
    var ready_pages_ids_arr   = [];
    var pages_urls_str = '\n';
    var x, y;


    Q.fcall(function () {
        debug.log(script_name,colors.bgMagenta.white('make_click2 ---------------  step #0 - wait'));
    }).delay(1000).then(function (res) {


        debug.log(script_name,colors.bgMagenta.white('make_click2 ---------------  step #1 - look pages'));

        for (var id in obj.pages) {
            if (obj.pages.hasOwnProperty(id) && obj.pages[id].hasOwnProperty('id')  && obj.pages[id]) {

                pages_count++;
                if (!obj.pages[id].loading && obj.pages[id].url) {
                    ready_pages_ids_arr.push(obj.pages[id].id);
                }

                pages_urls_str += '\t\t' + pages_count + '\tpage_id: ' + obj.pages[id].id + '\t' + obj.pages[id].url + ' loading=' + obj.pages[id].loading + ' progress= ' + obj.pages[id].loadingProgress + '\n';

                if (obj.pages[id].url && !obj.pages[id].loading) {
                    // проверяем есть ли тумбы ////////
                    thumb = random_thumb(obj.pages[id]);
                    debug.log(script_name, ' @ ', colors.bgBlack.white('checking tumbs = ', JSON.stringify(thumb)));
                    if (!thumb.hasOwnProperty('x')) {
                        debug.log(script_name,colors.bgCyan.red('no have tumbs page_id: ', obj.pages[id].id, 'url: ', obj.pages[id].url));
                        obj.pages[id].no_have_thumb = true;

                        if (obj.pages[id].id && obj.pages[id].url) {
                            debug.log(script_name, 'add page to array for pages_for_close, id:', obj.pages[id].id, 'page_id: ', obj.pages[id].id, obj.pages[id].url);
                            param.pages_for_close.push(obj.pages[id].id);
                        }
                    } else {

                        for (var i in param.pages_for_close) {
                            if (param.pages_for_close.hasOwnProperty(i) && param.pages_for_close[i]) {
                                if (param.pages_for_close[i] == obj.pages[id].id) {
                                    delete param.pages_for_close[i];
                                }
                            }
                        }

                        debug.log(script_name,colors.bgCyan.red('have tumbs page_id: ', obj.pages[id].id, 'url: ', obj.pages[id].url,'l:80'));
                        addToHistory({url: obj.pages[id].url, siteType: 'thumb', page: obj.pages[id].id}, obj);
                        debug.log(script_name,'00001-80');
                    }
                }

            }
            else if (obj.pages.hasOwnProperty(id) && obj.pages[id]) {
                pages_urls_str += '\t\t' + n / a + '\tpage_id: ' + obj.pages[id].id + '\t' + obj.pages[id].url + ' loading=' + obj.pages[id].loading + ' progress= ' + obj.pages[id].loadingProgress + '\n';
            } else {
                debug.log(script_name,'var id in obj.pages - null page detected');
            }
        }

        return pages_urls_str;

    }).delay(100).then(function (res) {

        debug.log(script_name,colors.bgMagenta.white('make_click2 ---------------  step #2 '), '  for loop - end, pages_count=', pages_count, '    ready_pages_ids_arr.len ', ready_pages_ids_arr.length);

        if (param.history) {
            if (param.history.hasOwnProperty('domains')) {
                var thumb_domains_count = param.history.domains.length;
            }
        }


        var pages_arr_count = 0;
        pages_id_arr        = [];
        for (var id in obj.pages) {
            if (obj.pages.hasOwnProperty(id)) {
                if (obj.pages[id].hasOwnProperty('id')) {
                    pages_id_arr[pages_arr_count] = obj.pages[id].id;
                    pages_arr_count++;
                }
            }
        }

        debug.log(script_name,colors.bgWhite.blue('@@@@@', '   open pages: ', pages_count, ' of ', param.max_open_pages, '    make clicks: ', param.task.click_ok, ' of ', param.task.click_task, '    thumb_domains_count: ', thumb_domains_count, '\n', pages_urls_str));


    }).delay(100).then(function (res) {

        debug.log(script_name,colors.bgMagenta.white('make_click2 ---------------  step #3 change obj_for_click ready_pages_ids_arr.length = '+ready_pages_ids_arr.length));

        //debug.log(script_name,'generate rand_page_count_id from 0 to '+pages_arr_count);
        var rand1 = random.int(0, ready_pages_ids_arr.length - 1);
        debug.log(script_name,'generate rand_page_count_id from 0 to ' + pages_id_arr.length - 1, ' = ', rand1);
        var rand_page_count_id = ready_pages_ids_arr[rand1];
        debug.log(script_name,'rand_page_count_id = ', rand_page_count_id);

        for (var id3 in obj.pages) {
            if (obj.pages.hasOwnProperty(id3)) {
                if (obj.pages[id3].hasOwnProperty('id')) {
                    if (obj.pages[id3].id == rand_page_count_id) {

                        debug.log(script_name,colors.bgGreen('make_click() 1', 'change obj_for_click \n',
                            '           from     page_id: ', obj_for_click.id, 'url: ', obj_for_click.url + '\n',
                            '           to       page_id: ', obj.pages[id3].id, 'url: ', obj.pages[id3].url + '\n'
                        ));

                        obj_for_click = obj.pages[id3];
                        break;
                    }
                }
            }
        }

        debug.log(script_name,colors.bgMagenta.white('make_click()  2', 'CHECK THUMBS...', 'page_id: ', obj_for_click.id, ' url= ', obj_for_click.url));

        return random_thumb(obj_for_click);


    }).delay(100).then(function (thumb) {

        debug.log(script_name,colors.bgMagenta.white('make_click2 ---------------  step #4 CHECK THUMBS... '));

        if (!thumb.x || !thumb.y) {
            debug.log(script_name,'mc022');
            debug.log(script_name,'make_click() no have thumbs in obj_for_click', 'obj_for_click.url', obj_for_click.url, 'change obj_for_click to  obj_main');
            obj_for_click = obj_main;

            return random_thumb(obj_for_click);

        } else {
            return thumb;
        }


    }).delay(100).then(function (thumb) {

        debug.log(script_name,colors.bgMagenta.white('make_click2 ---------------  step #5   random thumb selected = ', JSON.stringify(thumb)));

        if (!thumb.x || !thumb.y) {
            debug.log(script_name,'make_click() ', ' @ ', 'no have thumbs --> resolve status error,  page id,url: ', obj_for_click.id, obj_for_click.url);
            deferred.resolve({status: 'error'});
        } else {

            debug.log(script_name, colors.bgCyan.red('make_click()  ', ' tumbs have ', 'obj_for_click.url', obj_for_click.url, ' id', obj_for_click.id));
            addToHistory({url: obj_for_click.url, siteType: 'thumb', page: obj_for_click.id}, obj);
            param.have_thumb++;

            x = random.int(thumb.x + 5, thumb.x + thumb.width - 5);
            y = random.int(thumb.y + 5, thumb.y + thumb.height - 5);

            param.popupLoading = false;
            param.page_created = false;
        }

    }).delay(100).then(function () {

        debug.log(script_name,colors.bgMagenta.white('make_click2 ---------------  step #6  '));
        debug.log(script_name,colors.bgYellow.black('make_click() start clicking on page  xy:', x, y,' id, url:', obj_for_click.id, obj_for_click.url));

        if (pages_count > param.max_open_pages) { //if (rand_click_type == 1) {
            debug.log(script_name,'make_click()  ', 'pages_count > param.max_open_pages, start click no open new page click_xy');
            param.task.click_ok++;
            return click_xy(obj_for_click, x, y, 'left');
        }
        else {

            debug.log(script_name,colors.bgYellow.black('make_click() ', ' evalute() OPEN PAGE IN NEW WINDOW - start', 'from page_id: ', obj_for_click.id, 'click on the page :', obj_for_click.url));

            param.task.click_ok++;
            return obj_for_click.evaluate(function (x, y) {

                function rand(min, max) {
                    return Math.floor(Math.random() * (max - min + 1)) + min;
                }

                function getOffsetRect(elem) {
                    var box        = elem.getBoundingClientRect();
                    var body       = document.body;
                    var docElem    = document.documentElement;
                    var scrollTop  = window.pageYOffset || docElem.scrollTop || body.scrollTop;
                    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft;
                    var clientTop  = docElem.clientTop || body.clientTop || 0;
                    var clientLeft = docElem.clientLeft || body.clientLeft || 0;
                    var top        = box.top + scrollTop - clientTop;
                    var left       = box.left + scrollLeft - clientLeft;
                    var width      = elem.offsetWidth;
                    var height     = elem.offsetHeight;
                    return {el: elem, x: Math.round(left), y: Math.round(top), width: width, height: height}
                }

                var thumb_top_res = [];
                var mass          = document.querySelectorAll("a img");
                var sizes         = {};

                for (var i = 0; i < mass.length; i++) {
                    if (mass[i].height >= 50) {
                        var rect = getOffsetRect(mass[i]);
                        var size = rect.width + 'x' + rect.height;
                        if (typeof sizes[size] == 'undefined') sizes[size] = [];
                        sizes[size].push(mass[i]);
                    }
                }

                var len = 0;
                for (var size2 in sizes) {
                    if (len < sizes[size2].length) {
                        len           = sizes[size2].length;
                        thumb_top_res = sizes[size2].slice(0, 10);
                    }
                }

                console.log('thumb_top_res.length = ',thumb_top_res.length);

                var ev = document.createEvent("MouseEvent");
                var el = thumb_top_res[rand(0, thumb_top_res.length - 1)].parentNode;
                el.setAttribute('target', '_blank');
                ev.initMouseEvent(
                    "click",
                    true /* bubble */, true /* cancelable */,
                    window, null,
                    0, 0, 0, 0, /* coordinates */
                    false, false, false, false, /* modifier keys */
                    0 /*left*/, null
                );

                el.dispatchEvent(ev);

            }, x, y);


        }


    }).delay(500).then(function () {

        debug.log(script_name,colors.bgMagenta.white('make_click2 ---------------  step #7  '));
        debug.log(script_name,colors.bgYellow.black('make_click() ', ' clicking - finished'));

        if (pages_count > param.max_open_pages)
        {
            var pages_need_for_close = pages_count - param.max_open_pages;

            debug.log(script_name,colors.bgYellow.black('make_click() ', 'pages_need_for_close = '+pages_need_for_close+ ' param.max_open_pages = '+param.max_open_pages+ ' pages_count = '+pages_count));

            var p=null, all_pages_idx= [], rand_page_for_close=null;
            //
            debug.log(script_name,'закрываем страницы с одинаковым доменом');
            var page_domains = [];
            for (p in obj.pages) {
                if (obj.pages.hasOwnProperty(p) && obj.pages[p].id && obj.pages[p].url) {

                    var page_domain = domain.getDomain(obj.pages[p].url);

                    if (page_domains.indexOf(page_domain) >=0 &&
                        obj_for_click.id !== obj.pages[p].id) {
                        param.pages_for_close.push(obj.pages[p].id);
                        pages_need_for_close--;
                        debug.log(script_name,colors.red('dublicate domains -> pages_for_close page_domains' + JSON.stringify(page_domains) + ' page_id: ' + obj.pages[p].id + ' page_domain ' + page_domain + ' url ' + obj.pages[p].url))
                    }
                    page_domains.push(page_domain);
                }
            }



            if (pages_need_for_close>0) {
                debug.log(script_name,colors.bgYellow.black('make_click() '), 'ищем рандомную страницу для закрытия (1)');

                all_pages_idx = [];

                for (p in obj.pages) {
                    if (obj.pages.hasOwnProperty(p) && obj.pages[p] && obj.pages[p].hasOwnProperty('id')
                    && obj.pages[p].url /*&& !obj.pages[p].loading*/
                    ) {
                        all_pages_idx.push(p);
                        pages_need_for_close--;
                    }
                }

                if (all_pages_idx.length>0) {
                    rand_idx_page_for_close = all_pages_idx[random.int(0, all_pages_idx.length - 1)];

                    debug.log(script_name,colors.bgYellow.black('make_click() ', 'закрываем рандомную страницу id ' + obj.pages[rand_idx_page_for_close].id));

                    param.pages_for_close.push(obj.pages[rand_idx_page_for_close].id);
                    debug.log(script_name,colors.red('pages_count > param.max_open_pages -> random pages_for_close ' + obj.pages[rand_idx_page_for_close].id + ' ' + obj.pages[rand_idx_page_for_close].url));
                }
            }


        }


    }).delay(100).then(function () {
        return check_pages(obj_main);
    }).catch(function (err) {
        error('!!! process.exit !!!, make_click2  catch err ' + JSON.stringify(err));
        process.exit(11)

    }).delay(500).done(function () {
        debug.log(script_name,colors.bgMagenta.white('make_click2 -  step #5 DONE '));

        deferred.resolve({status: 'success'});

    });


    return deferred.promise;

};


//wait for something
function waitFor(testFx, onReady, timeOut, timeFrequency) {
    var maxtimeOutMillis = timeOut ? timeOut : 3000,
        start            = new Date().getTime(),
        condition        = false,
        interval         = setInterval(function () {
            if ((new Date().getTime() - start < maxtimeOutMillis) && !condition) {
                condition = (typeof(testFx) === "string" ? eval(testFx) : testFx());
            } else {
                if (!condition) {
                    //debug.log(script_name,"'waitFor()' timeout");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                } else {
                    //debug.log(script_name,"'waitFor()' finished in " + (new Date().getTime() - start) + "ms.");
                    clearInterval(interval);
                    typeof(onReady) === "string" ? eval(onReady) : onReady();
                }
            }
        }, timeFrequency);
}

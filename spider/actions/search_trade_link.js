var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var domain   = require('./../lib/domain');
var Q       = require('./../modules/q/q.js');

var colors  = require('./../modules/colors/safe.js');
//var make_click  = require('./make_click').exec;
var get_all_links  = require('./get_all_links').exec;

var script_name = 'search_trade_link() ';

var tradelinks = param.tradelinks;

var result;

exports.exec = function(obj){

    var deferred = Q.defer();
    var page_url = obj.url;
    var page_domain = domain.getDomain(page_url);

    Q.fcall(function(){
        debug.log(script_name, 'start');

        debug.log(obj.te)

        return get_all_links(obj,'with_innerText');

    }).then(function(all_links){

        debug.log(script_name, '### STEP: 2');

        debug.log(script_name, 'all_links.length ',all_links.length);
        //debug.log(script_name, 'all_links = ',JSON.stringify(all_links));

        var show_links_amount = 30;
        var trade_link_url, trade_link_text;
        var trade_links =[];
        var trade_link_found =[];

        search_loop_text:
        for (var link_idx = all_links.length-1; link_idx > all_links.length-show_links_amount-1 && link_idx>=0; link_idx--) {
            if (all_links.hasOwnProperty(link_idx)) {
                debug.log(script_name, 'all_links', all_links[link_idx].innerText, all_links[link_idx].href);

                trade_links.push({idx: link_idx, text: all_links[link_idx].innerText, link: all_links[link_idx].href});


                if (tradelinks && tradelinks.texts && tradelinks.texts.length >0) {
                    for (var i in tradelinks.texts) {
                        if (tradelinks.texts[i]) {
                            var search_text = tradelinks.texts[i];

                            debug.log(script_name,'seach trade link search_text = '+search_text,' in '+all_links[link_idx].innerText);

                            if (all_links[link_idx].innerText &&
                                all_links[link_idx].innerText.toLowerCase().match(new RegExp(search_text, 'i'))) { // 'i' modifier that means "ignore case"
                                debug.log(script_name, 'all_links', '$$$$$$$$ trade link find', all_links[link_idx].innerText, all_links[link_idx].href);
                                trade_link_url  = all_links[link_idx].href;
                                trade_link_text = all_links[link_idx].innerText;
                                trade_link_found.push({
                                    idx:  link_idx,
                                    text: all_links[link_idx].innerText,
                                    link: all_links[link_idx].href,
                                    finded_by: 'search_text = '+search_text
                                });
                                break search_loop_text;
                            }


                        }
                    }
                } else {
                    console.log('have not tradelinks.texts');
                }


            }
        }


        if (!trade_link_url) {
            search_loop_href:
            for (var link_idx = all_links.length - 1; link_idx > all_links.length - show_links_amount - 1 && link_idx >= 0; link_idx--) {
                if (all_links.hasOwnProperty(link_idx)) {
                    debug.log(script_name, 'all_links', all_links[link_idx].innerText, all_links[link_idx].href);


                    if (tradelinks && tradelinks.hrefs && tradelinks.hrefs.length > 0) {
                        for (var i in tradelinks.hrefs) {
                            if (tradelinks.hrefs[i]) {
                                var search_hrefs = tradelinks.hrefs[i];

                                debug.log(script_name, 'seach trade link search_hrefs = ' + search_hrefs, ' in ' + all_links[link_idx].href);

                                if (all_links[link_idx].href &&
                                    all_links[link_idx].href.toLowerCase().match(new RegExp(search_hrefs, 'i'))) { // 'i' modifier that means "ignore case"
                                    debug.log(script_name, 'all_links', '$$$$$$$$ trade link find', all_links[link_idx].innerText, all_links[link_idx].href);
                                    trade_link_url  = all_links[link_idx].href;
                                    trade_link_text = all_links[link_idx].innerText;
                                    trade_link_found.push({
                                        idx:       link_idx,
                                        text:      all_links[link_idx].innerText,
                                        link:      all_links[link_idx].href,
                                        finded_by: 'search_hrefs = ' + search_hrefs
                                    });
                                    break search_loop_href;
                                }


                            }
                        }
                    } else {
                        console.log('have not tradelinks.hrefs');
                    }

                }
            }
        }

        if (trade_link_url) {

            result = {
                status: 'success',
                result: {
                    text: trade_link_text,
                    link: trade_link_url,
                    trade_link_found: trade_link_found
                }
            };

            // addTohistory page_url

            var finded= false;
            var htl = param.uniq_history_urls.urls_with_trade_link;
            for (var i in htl) {
                if (htl.hasOwnProperty(i)) {
                    if (htl[i].domain == page_domain) {
                        finded= true;
                        break;
                    }
                }
            }
            if (!finded && page_domain !== param.task.trader) {
                param.uniq_history_urls.count_with_trade_link++;
                param.uniq_history_urls.urls_with_trade_link.push({
                    domain:  page_domain,
                    url:  page_url,
                    text: trade_link_text,
                    link: trade_link_url
                });

                param.urls_trade.push(page_domain)
            }



            return result;

        } else {

            result = {
                status: 'TRADE_LINK_NOT_FOUND',
                result: trade_links
            };

            return result;
        }


    }).catch(function (e) {

        debug.log(colors.bgWhite.red(script_name, '### STEP: catch'));
        debug.log(colors.bgWhite.red(script_name,'catch',e.message));

        deferred.reject(e);

    }).done(function(result){





        debug.log(script_name, 'end');

        deferred.resolve(result);

    });



    //debug.log('search_trade_link()  ', 'return deferred.promise');

    return deferred.promise;

};


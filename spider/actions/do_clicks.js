var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q.js');

var colors  = require('./../modules/colors/safe.js');
var make_click  = require('./make_click2').exec;

exports.exec = function(obj, x, y, button){

    var deferred = Q.defer();

    var task_finished = false;
    var try_count_max = config.const.try_count_max + param.task.click_task;
    var click_task = param.task.click_task;

    var try_count = 0;
    promiseWhile(function () { return !task_finished; }, function (result) {

        try_count++;

        debug.log('do_clicks()  ', 'obj.url=', obj.url, 'obj.id=',obj.id);


        if(try_count <= try_count_max){
            debug.log('');
            debug.log('do_clicks()  ','Running task try:', try_count+'/'+try_count_max, 'clicks:', param.task.click_ok+'/'+click_task);
        }

        //is it enough?
        if(param.task.click_ok >= click_task) {

            debug.log('do_clicks()  ',' clicks enought, finished.');

            deferred.resolve({
                status: 'success'
            });
            task_finished = true;
            return;
        }

        //reached the limit?
        if(try_count > try_count_max){

            debug.log('do_clicks()  ',' tryings reached the max value, finished.');

            deferred.resolve({
                status: 'success'
            });
            task_finished = true;
            return;
        }
        debug.log('do_clicks()  ', 'return make_click(obj)');
        return make_click(obj);

    }).then(function () {

        debug.log('do_clicks()  ',' error, finished.');

        deferred.resolve({
            status: 'error'
        });

    }).done();

    debug.log('do_clicks()  ', 'return deferred.promise');

    return deferred.promise;

};

//while loop with promises
function promiseWhile(condition, body) {
    var done = Q.defer();
    function loop() {
        if (!condition()) return done.resolve();
        Q.when(body(), loop, done.reject);
    }
    Q.nextTick(loop);
    return done.promise;
}

var config  = require('./../inc/config');
var param   = require('./../inc/param');
//var time    = require('./../lib/time');
var debug   = require('./../lib/debug');
var random  = require('./../lib/random');
var Q       = require('./../modules/q/q.js');
var fs      = require('fs');

var script_name = 'make_screen()';

exports.exec = function(obj){

    debug.log('');
    debug.log(script_name, 'Make screenshot:', obj.id, obj.url);

    //var deferred = Q.defer();

    /*obj.evaluate(function() {
        document.body.bgColor = 'black';
    });*/
    var screenshot_dir = '/usr/share/biterika/spider/screens/';

    if ( !fs.isDirectory(screenshot_dir) ) {
        debug.log(script_name, 'creating dir: '+screenshot_dir);
        fs.makeDirectory(screenshot_dir);
    }

    var filename = screenshot_dir+'/'+Date.now()+'-'+obj.id+'-'+random.char(4)+'.jpg';

    debug.log(script_name, 'render '+filename);
    obj.render(filename, {format: 'jpeg', quality: '40'});

    //var content = fs.read(filename);
    //debug.log('send screen',content.length);
    //TODO send
    //fs.remove(filename);

    var result = {
        status: 'success'
    };
    //deferred.resolve(result);

    //return deferred.promise;

};

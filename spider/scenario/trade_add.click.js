var system  = require('system');
var args    = system.args;

//Libs
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var Q       = require('./../modules/q/q');
//var page    = require('webpage').create();

//Actions
var check_popup = require('./../actions/check_popup').exec;
var do_clicks   = require('./../actions/do_clicks').exec;
//var postload    = require('./../actions/postload').exec;
var colors  = require('./../modules/colors/safe.js');
var make_screen    = require('./../actions/make_screen').exec;

var script_name = 'trader_add.click.js';

exports.exec = function(page){

    debug.log(script_name, 'start');

    var deferred = Q.defer();

    Q.fcall(function(){
        debug.log(script_name,'start');

        debug.log(script_name,'open page ',param.task.trader_url);
        return open_page(page, param.task.trader_url);

    }).delay(2000).then(function () {
        debug.log(script_name,'<- page opened ',page.url);
        make_screen(page);
        debug.log(script_name,'param.task.click_task ',param.task.click_task, 'param.task.click_ok',param.task.click_ok);
        debug.log(' ');
        debug.log(script_name,'-> check_popup...');

        return check_popup(page);

    }).delay(2000).then(function () {


        debug.log(' ');
        debug.log(' ');
        debug.log(colors.red.underline('===================> do_clicks START ================================='));
        debug.log(' ');
        debug.log(' ');

        return do_clicks(page);


    }).delay(2000).then(function () {


        var click_task = param.task.click_task;
        var click_ok = param.task.click_ok;
        var code = '';
        debug.log(' ');
        debug.log(' ');
        debug.log(colors.red.underline('===================> do_clicks FINISH ================================='));
        debug.log(' ');
        debug.log(' ');
        if(param.have_thumb == 0){
            debug.log(script_name,'NOT_FOUND_THUMB');
            code = 'NOT_FOUND_THUMB';

        }else if(click_task > 0 && click_task == click_ok){

            debug.log(script_name,'TASK_FINISH_FULL','click_task=',click_task, 'click_ok=',click_ok);
            code = 'TASK_FINISH_FULL';

        }else if(click_ok > 0 && click_ok < click_task){

            debug.log(script_name,'TASK_FINISH_BAD_1','click_task=',click_task, 'click_ok=',click_ok);
            code = 'PART_POPUP';

        }else{
            debug.log(script_name,'TASK_FINISH_BAD_2','click_task=',click_task, 'click_ok=',click_ok);
            code = 'NO_HAVE_POPUP';

        }


        deferred.resolve(code);


    }).catch(function (e) {

        debug.log(colors.bgWhite.red(script_name,' catch: ',JSON.stringify(e)));
        deferred.reject(e);


    }).delay(2000).done(function(){
        debug.log(script_name, 'start');
    });

    debug.log('spider.biterika', 'deferred.promise');

    return deferred.promise;

};

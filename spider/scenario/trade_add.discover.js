var system  = require('system');
var args    = system.args;

//Libs
var config  = require('./../inc/config');
var param   = require('./../inc/param');
var debug   = require('./../lib/debug');
var domain   = require('./../lib/domain');
var colors  = require('./../modules/colors/safe.js');
var Q       = require('./../modules/q/q');
//var page    = require('webpage').create();

//Actions
//var check_popup = require('./../actions/check_popup').exec;
//var do_clicks   = require('./../actions/do_clicks').exec;
var search_trade_link   = require('./../actions/search_trade_link').exec;
//var postload    = require('./../actions/postload').exec;


var make_screen    = require('./../actions/make_screen').exec;
//var get_position    = require('./../actions/get_position').exec;
//var waitPageLoad        = require('./../actions/waitPageLoad').exec;
var get_form_data        = require('./../actions/get_form_data').exec;
//var get_textContent_Page        = require('./../actions/get_textContent_Page').exec;
var get_category_link        = require('./../actions/get_category_link').exec;
var get_match_category        = require('./../actions/get_match_category').exec;
var open_page        = require('./../actions/open_page').exec;
var check_popup = require('./../actions/check_popup').exec;


var script_name = colors.bgWhite.magenta('trade_add.discover.js');

var code;

var cache       = {};

var results = {
    CATEGORY_LINK: {
        have: false,
        array: []
    },
    CATEGORY_MATCH: {
        have: false,
        array: []
    },
    SITE_TYPES: {
        have: false,
        array: {}
    },
    TRADE_LINK: {
        have: false,
        array: []
    },
    TRADE_FORM: {
        have: false,
        array: []
    }
};

exports.exec = function(page){


    var deferred = Q.defer();

    var exit_case = null;

    var trade_form_url;

    Q.fcall(function(){
        debug.log(script_name,'start');

        debug.log(script_name,'open page ',param.task.trader_url);
        return open_page(page, param.task.trader_url,'make_new_page');

    }).delay(400).then(function (page_new) {
        page = page_new;
        debug.log(script_name,'<- page opened ',page.url);
        debug.log(' ');
        debug.log(script_name,'-> check_popup...');

        return check_popup(page);

    }).delay(400).then(function () {
        debug.log(script_name,'<- page opened ',page.url);

        if (page.url && domain.getDomain(page.url) &&  domain.getDomain(page.url) !== domain.getDomain(param.task.trader_url)) {
            console.log('EXIT_CASE => REDIRECT_TO_ANOTHER_DOMAIN');
            exit_case = 'REDIRECT_TO_ANOTHER_DOMAIN';
            return 'EXIT_CASE';
        } else if (!page.content || page.content.length ==0 || page.content=='<html><head></head><body></body></html>') {
            console.log('EXIT_CASE => PAGE_OPEN_EMPTY');
            exit_case = 'PAGE_OPEN_EMPTY';
            return 'EXIT_CASE';
        } else {
            console.log('[PAGE_OPENED OKAY] content length = '+page.content.length,'content = ',show_part(page.content));
        }

        if (param.task.task_origin.make_screen) {
            make_screen(page);
        }

        debug.log(' ');
        debug.log(script_name,' search_trade_link .... ');


        return search_trade_link(page);

    }).delay(400).then(function (trade_link) {
        if (exit_case) return 'EXIT_CASE';

        if (trade_link && trade_link.result) {
            param.task.results.TRADE_LINK = {};
            param.task.results.TRADE_LINK.array = trade_link.result;
        }

        if (trade_link &&
            trade_link.hasOwnProperty('status') &&
            trade_link.status=='success'
        ) {
            debug.log(script_name, '<- [TRADE_LINK] ['+JSON.stringify(trade_link.result)+']');
            results.TRADE_LINK.have = true;
            results.TRADE_LINK.array = trade_link.result;
            trade_form_url = trade_link.result.link;

            if (trade_form_url.match(/^mailto*./)){
                console.log('trader link wrong - its email');
                exit_case = 'TRADE_LINK_NOT_FOUND';
                return 'EXIT_CASE';
            }

            return results.TRADE_LINK.array.link;

        }
        else {
            debug.log(script_name, '<- [TRADE_LINK_NOT_FOUND] ['+JSON.stringify(trade_link.result)+']');
            results.TRADE_LINK.array = trade_link.result;

            exit_case = 'TRADE_LINK_NOT_FOUND';

            return 'EXIT_CASE';
        }

    }).delay(400).then(function (res) {
        if (exit_case) return 'EXIT_CASE';

        debug.log(script_name,'-> try get category link...');
        return get_category_link(page);

    }).delay(400).then(function (category_links) {
        if (exit_case) return 'EXIT_CASE';

        if (category_links &&
            category_links.hasOwnProperty('length') &&
            category_links.length>0
        ) {
            debug.log(script_name, '<- [CATEGORY_LINK] ['+JSON.stringify(category_links)+']');
            results.CATEGORY_LINK.have = true;
            results.CATEGORY_LINK.array = category_links;


        } else {
            debug.log(script_name, '<- [CATEGORY_LINK_NOT_FOUND] ['+JSON.stringify(category_links)+']');
        }

        debug.log(' ');
        debug.log(script_name,'-> try detect categories at this page...');

        return get_match_category(page);

    }).delay(400).then(function (match_category) {
        if (exit_case) return 'EXIT_CASE';

        if (match_category &&
            match_category.hasOwnProperty('length') &&
            match_category.length>0
        ) {
            debug.log(script_name, '<- [CATEGORY_MATCH] ['+JSON.stringify(match_category)+']');

            results.CATEGORY_MATCH.have = true;
            results.CATEGORY_MATCH.array = match_category;


/*
            if (results.SITE_TYPES.array.site_types &&
                match_category.hasOwnProperty('length') &&
                match_category.length>0
            ) {
                results.SITE_TYPES.have = true;
            }*/


        }
        else {
            debug.log(script_name, '<- [CATEGORY_MATCH_NOT_FOUND] ['+JSON.stringify(match_category)+']');
        }

        debug.log(' ');
        debug.log(script_name,'-> check no_trade text on page...');

        var pageText = page.plainText;
        for (var cl in param.closed_list) {
            if (param.closed_list.hasOwnProperty(cl)) {
                if (pageText.toString().match(param.closed_list[cl])) {
                    debug.log(script_name,'[no_trade] match closed_list closed_list[cl] = '+JSON.stringify(param.closed_list[cl]));
                    param.closed_list_match = true;
                }
            }
        }

        return true;

    }).delay(400).then(function (res) {
        if (exit_case) return 'EXIT_CASE';

        if (results.TRADE_LINK.have) {

            debug.log(' ');
            debug.log(script_name,'-> try to open trade form page...');

            return open_page(page, trade_form_url,'make_new_page');
        }


    }).delay(400).then(function (page_new) {
        if (exit_case) return 'EXIT_CASE';
        page = page_new;
        debug.log(script_name,'<- page opened ',page.url);

        if (!page.content || page.content.length ==0 || page.content=='<html><head></head><body></body></html>') {
            console.log('EXIT_CASE => PAGE_OPEN_EMPTY');
            exit_case = 'PAGE_OPEN_EMPTY';
            return 'EXIT_CASE';
        } else {
            console.log('[PAGE_OPENED OKAY] content length = '+page.content.length, 'content = ',show_part(page.content));
        }

        debug.log(' ');
        debug.log(script_name,'-> check_popup...');

        return check_popup(page);

    }).delay(400).then(function () {
        if (exit_case) return 'EXIT_CASE';

        if (results.TRADE_LINK.have) {
            debug.log(script_name,'<- trade form page is open');
            debug.log(' ');
            debug.log(script_name,'-> try to get forms data...');

            return get_form_data(page);
        }


    }).delay(400).then(function (form_data_res) {
        if (exit_case) return 'EXIT_CASE';

        param.task.results = results;

        if (results.TRADE_LINK.have) {

            if (form_data_res &&
                form_data_res.hasOwnProperty('form_data') &&
                form_data_res.form_data.length > 0
            ) {

                debug.log(script_name, '<- [TRADE_FORM] [' + JSON.stringify(form_data_res.form_data) + ']');
                results.TRADE_FORM.have = true;
                results.TRADE_FORM.array = {link: page.url, fields: form_data_res.form_data};

            } else {

                debug.log(script_name, '<- [TRADE_FORM_NOT_FOUND] [' + page.url + ']');
                debug.log(script_name,'-> try make screenshot...');
                page.clipRect = { top: 0, left: 0, width: 1280, height: 1280 };
                page.viewportSize = {
                    width:  param.task.sx || 1366,
                    height: param.task.sy || 768
                };
                param.task.filled_form_screenshot_base64 = page.renderBase64('JPEG');
                param.task.screen_url = page.url||null;
                exit_case = 'TRADE_FORM_NOT_FOUND';
                results.TRADE_FORM.array = {link: page.url, fields:[] };
            }
        }

    }).delay(400).then(function (form_data_res) {
        if (exit_case) return 'EXIT_CASE';

        if (results.TRADE_FORM.have) {

            if (param.task.task_type_origin=='test' ||
                param.task.task_type_origin=='test2' ||
                param.task.task_type_origin=='add'
            )
            {
                param.task.sub_scenario = 'trade_add.fill_form';

                debug.log(script_name, '-> start sub_sceanrio ',param.task.sub_scenario);
                cache[param.task.sub_scenario] = require('./'+param.task.sub_scenario).exec;

                return cache[param.task.sub_scenario](page);
            }

        }


    }).delay(400).then(function (res) {
        if (exit_case) return 'EXIT_CASE';

        debug.log(script_name, '<- start sub_sceanrio finished ',param.task.sub_scenario);

    }).catch(function (e) {

        debug.log(colors.bgWhite.red(script_name,' catch: ',JSON.stringify(e)));
        deferred.reject(e);
        //deferred.resolve(e);


    }).done(function(){
        debug.log(script_name, 'done(function()');

        if (exit_case) param.exit_case = exit_case;


        debug.log(script_name, '[RESULTS] ['+JSON.stringify(param.task.results)+']');

        code = 'TASK_FINISH';
        deferred.resolve({status: 'success', code: code, result: param.task.results});

    });

    //debug.log('search_tradeForm.biterika.js', 'deferred.promise');

    return deferred.promise;

};



function show_part(text){
    if (text.length <= 256 ) return text;
    var text_start = text.substr(0,128);
    var text_end = text.substr(text.length-128,128);

    return text_start+' ... '+text_end;
}
var system = require('system');
var args   = system.args;

//Libs
var config = require('./../inc/config');
var param  = require('./../inc/param');
var debug  = require('./../lib/debug');
var colors = require('./../modules/colors/safe.js');
var Q      = require('./../modules/q/q');
//var page    = require('webpage').create();

//Actions
//var check_popup = require('./../actions/check_popup').exec;
//var do_clicks   = require('./../actions/do_clicks').exec;
//var search_trade_link   = require('./../actions/search_trade_link').exec;
//var postload    = require('./../actions/postload').exec;

//var get_captcha = require('./../actions/get_captcha').exec;
var captcha = require('./../actions/captcha').exec;


var make_screen = require('./../actions/make_screen').exec;
//var get_position    = require('./../actions/get_position').exec;
//var waitPageLoad        = require('./../actions/waitPageLoad').exec;
//var get_form_data        = require('./../actions/get_form_data').exec;
//var get_textContent_Page        = require('./../actions/get_textContent_Page').exec;
//var get_category_link        = require('./../actions/get_category_link').exec;
//var get_match_category        = require('./../actions/get_match_category').exec;
//var open_page        = require('./../actions/open_page').exec;
var fill_form            = require('./../actions/fill_form').exec;
var get_textContent_Page = require('./../actions/get_textContent_Page').exec;


var script_name = colors.bgWhite.magenta('trade_add.fill_form.js');

var code;

var results = {};

exports.exec = function (page) {


    var deferred = Q.defer();

    Q.fcall(function () {

        debug.log(script_name, 'start');
        debug.log(script_name, 'now page url ', page.url, ' check task_origin.captcha', JSON.stringify(param.task.task_origin));

        if (param.task.task_origin.captcha) {
            debug.log(script_name, ' captcha ', JSON.stringify(param.task.task_origin.captcha));
        }

        if (param.task.task_origin.captcha) {
            return captcha(page, param.task.task_origin.captcha);
        }

        return false;


    }).delay(2000).then(function (captcha_result) {


        if (captcha_result) {

            debug.log(script_name, 'get_captcha_result = ', JSON.stringify(captcha_result));

            var captcha_value_finded = false;

            for (var i in param.task.fields) {
                if (param.task.fields.hasOwnProperty(i)) {
                    if (param.task.fields[i].value && param.task.fields[i].value == 'captcha') {
                        param.task.fields[i].value = captcha_result;
                        debug.log(script_name, 'captcha_value_finded, set answer ' + JSON.stringify(captcha_result));
                        captcha_value_finded = true;
                        break;
                    }
                }
            }

            if (!captcha_value_finded) {
                debug.log(script_name, '[ERROR] captcha_value_finded is not finded');
            }

        }

        debug.log(script_name, 'now page url ', page.url, '-> try fill form...');

        // пытаемся заполнить форму
        debug.log('заполняем форму данными ' + JSON.stringify(param.task.fields, '', '\t'));
        debug.log(script_name, 'start fill form by data: ', JSON.stringify(param.task.fields));

        return fill_form(page, param.task.fields);

    }).delay(7000).then(function (fill_form_result) {

        debug.log(script_name, '<- fill form finished');
        debug.log(script_name, 'finish fill form with result: ', JSON.stringify(fill_form_result));

        debug.log(' ');
        debug.log(script_name, '-> try make screenshot...');
        page.clipRect     = {top: 0, left: 0, width: 1366, height: 1280};
        page.viewportSize = {
            width:  param.task.sx || 1366,
            height: param.task.sy || 768
        };


        make_screen(page);
        param.task.filled_form_screenshot_base64              = page.renderBase64('JPEG');
        param.task.results.filled_form_screenshot_base64_FLAG = true;
        param.task.screen_url                                 = page.url || null;

        debug.log(script_name, 'form_screenshot_base64.length', param.task.filled_form_screenshot_base64.length);

    }).delay(2000).then(function () {

        debug.log(script_name, '<-  make screenshot finished');
        debug.log(' ');


        param.task.url_bfSend = page.url;

        if (param.task.task_sub_type == 'add') {

            debug.log(script_name, '->  try send form  and wait 10sec');
            // пытаемся отправить форму
            //debug.log(script_name, 'start send form by data: ', JSON.stringify(param.task.fields));
            return fill_form(page, param.task.fields, 'submit');

        }

    }).delay(10000).then(function (send_form_result) {


        //param.task.result_data.form_screenshot_base64 = page.renderBase64('JPEG');

        if (param.task.task_sub_type == 'add') {
            make_screen(page);
            debug.log(script_name, 'finish send form with result: ', JSON.stringify(send_form_result));

            if (page.url !== param.task.url_bfSend) {
                //param.task.add_status = 1;
            }
            var text_content_after_submit = get_textContent_Page(page);

            var result_success_message = null;
            param.task.add_status      = 0;
            param.task.add_reason      = '';

            /*
            status - 0 или 1 в зависимости от того удалось ли отправить форму

            если =0
reason - причина невыполнения задания (SITE_OPEN_FAIL, SITE_ALREADY_ADDED, WRONG_MESSAGE)
	SITE_OPEN_FAIL - сайт не открылся
	SITE_ALREADY_ADDED - сообщение что сайт уже был добавлен
	WRONG_MESSAGE - не удалось добавиться, не найдено сообщение об успешном добавлении, форма не отправилась, что угодно, что значит что мы отправим сайт заново на дискавери

            */


            if (
                param.task.success_message && text_content_after_submit &&
                text_content_after_submit.toString().match(param.task.success_message)
            ) {
                console.log('\n\n  SUCCESS ADD - result_success_message = 1   text_content_after_submit.toString().match(param.task.success_message) ' ,
                'param.task.success_message = ',param.task.success_message, '  text_content_after_submit.toString() = ',text_content_after_submit.toString(),'\n\n');
                result_success_message = 1;
                param.task.add_status  = 1;

            } else {

                if (
                    param.task.task_origin.already_added_message
                    && text_content_after_submit.toString().match(param.task.task_origin.already_added_message)
                ) {
                    console.log('\n\n  SITE_ALREADY_ADDED   ---  ' ,
                        'param.task.task_origin.already_added_message = ',param.task.task_origin.already_added_message, '  text_content_after_submit.toString() = ',text_content_after_submit.toString(),'\n\n');
                    result_success_message = 0;
                    param.task.add_status  = 0;
                    param.task.fail_reason  = 'SITE_ALREADY_ADDED';
                } else {
                    console.log('\n\n  WRONG_MESSAGE result_success_message = 0  ' ,
                        '  text_content_after_submit.toString() = ',text_content_after_submit.toString(),'\n\n');
                    result_success_message = 0;
                    param.task.add_status  = 0;
                    param.task.fail_reason = 'WRONG_MESSAGE';
                }

            }


            param.task.results.TRADE_FORM_SEND = {
                have:  true,
                array: {
                    form_url:               param.task.url_bfSend,
                    form_url_after_submit:  page.url,
                    textPage_after_submit:  text_content_after_submit,
                    success_message:        param.task.success_message || null,
                    already_add_message:    param.task.task_origin.already_added_message || null,
                    result_success_message: result_success_message
                }
            };

            debug.log(script_name, '[TRADE_FORM_SEND]: [' + JSON.stringify(param.task.results.TRADE_FORM_SEND) + ']');


        }


    }).catch(function (e) {

        debug.log(colors.bgWhite.red(script_name, ' catch: ', JSON.stringify(e)));
        //deferred.reject(e);
        deferred.reject(e);


    }).done(function () {
        debug.log(script_name, 'done(function()');

        debug.log(script_name, '[RESULTS] [' + JSON.stringify(param.task.results) + ']');

        code = 'TASK_FINISH';
        deferred.resolve({status: 'success', code: code, result: param.task.results});

    });

    //debug.log('search_tradeForm.biterika.js', 'deferred.promise');

    return deferred.promise;

};

var system = require('system');
var args   = system.args;

//Libs
var param = require('./inc/param');

var Q      = require('./modules/q/q');
var colors = require('./modules/colors/safe.js');

var debug   = require('./lib/debug');
var time   = require('./lib/time');
var webpage = require('webpage');
var page    = null;//webpage.create();
var events  = require('./events').exec;

var script_name = 'index.js';

//======================================================================================================================

console.log('args = ',JSON.stringify(args,'','\t'));

//if(typeof args[1] != 'undefined') param.task_url = args[1];
if (typeof args[1] != 'undefined') param.task_json = args[1];
if (typeof args[2] != 'undefined') param.finish_url = args[2];
if (typeof args[3] != 'undefined') param.closed_list = JSON.parse(args[3]);
if (typeof args[4] != 'undefined') param.tradelinks = JSON.parse(args[4]);


// param.task.disable_img_js = true;

param.uniq_history_urls.ts_start= time.time_int();

/*
system.stdout.writeLine('<call_agent>nodejs_reuest<call_agent/>');
var input = system.stdin.readLine();
if (input) {
    console.log('phantom js recived stdin input = ',JSON.stringify(input))
}
*/


var cache = {};
console.log(' ');
console.log(' ');
debug.log('#################################     SPIDER START     ####################################');
console.log(' ');
console.log(' ');
// debug.log('task_url', param.task_url);
// debug.log('finish_url', param.finish_url);


//======================================================================================================================

var open_page          = require('./actions/open_page').exec;
var get_task_from_json = require('./actions/get_task_from_json').exec;
var task_finish_post   = require('./actions/task_finish_post').exec;

//======================================================================================================================

phantom.onError = function (msg, trace) {
    var msgStack = ['PHANTOM ERROR: ' + msg];
    if (trace && trace.length) {
        msgStack.push('TRACE:');
        trace.forEach(function (t) {
            msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function + ')' : ''));
        });
    }
    debug.log(msgStack.join('\n'));
    //phantom.exit(1);
};

run_action();

function run_action() {


    Q.fcall(function () {

        debug.log(script_name, colors.bgWhite.blue.bold('### step 1 - init ###                                '));
        return init();

    }).delay(1000).then(function () {

        debug.log(script_name, colors.bgWhite.blue.bold('### step 2 - init events ###                         '));
        return events(page);

    }).delay(2000).then(function () {

        debug.log(script_name, colors.bgWhite.blue.bold('### step 3 - open task url ###                       '));
        return param.task_json;

    }).delay(2000).then(function () {

        debug.log(script_name, 'page.plainText', page.plainText);
        debug.log(script_name, colors.bgWhite.blue.bold('### step 4 - resolve task -> get_task_from_json  ### '));
        return get_task_from_json(param.task_json,page);

    }).delay(2000).then(function () {

        debug.log(script_name, colors.bgWhite.blue.bold('### step 5 - run scenario  ###                       '));
        debug.log(script_name, colors.bgWhite.blue.bold('        SCENARIO START:   ' + param.task.task_type + '   '));
        debug.log(script_name, 'param.task.task_type         start ', param.task.task_type);
        debug.log(script_name, 'param.task.task_type_origin  start ', param.task.task_type_origin);

        cache[param.task.task_type] = require('./scenario/' + param.task.task_type).exec;

        return cache[param.task.task_type](page);

    }).delay(2000).then(function () {

        debug.log(script_name, 'param.task.task_type finish ', param.task.task_type);
        debug.log(script_name, colors.bgWhite.blue.bold('        SCENARIO FINISH:   ' + param.task.task_type + '        '));
        debug.log(script_name, colors.bgWhite.blue.bold('### step 6 - send task_result data  ###  '));
        debug.log(script_name, '[TASK_RESULTS] [' + JSON.stringify(param.task.results) + ']');

        debug.log(script_name, 'call task_finished_post()...');

        return task_finish_post(page, param.task.results);

    }).catch(function (e) {

        if (e && e.hasOwnProperty('status') && e.status == 'NO_TASK') {
            console.log('[NO_TASK]');
            phantom.exit();
        }

        else if (e && e.hasOwnProperty('status') && e.status == 'SITE_OPEN_FAIL') {
            console.log('[SITE_OPEN_FAIL]');
            param.exit_case = 'SITE_OPEN_FAIL';
            return task_finish_post(page, param.task.results);
            // phantom.exit();
        }
        else if (e && e.hasOwnProperty('status') && e.status == 'REDIRECT_TO_ANOTHER_DOMAIN') {
            console.log('[REDIRECT_TO_ANOTHER_DOMAIN]');
            param.exit_case = 'REDIRECT_TO_ANOTHER_DOMAIN';
            return task_finish_post(page, param.task.results);
            // phantom.exit();
        }
        else if (e && e.hasOwnProperty('status') && e.status == 'PAGE_OPEN_TIMEOUT') {
            console.log('[PAGE_OPEN_TIMEOUT]');
            param.exit_case = 'PAGE_OPEN_TIMEOUT';
            return task_finish_post(page, param.task.results);
            // phantom.exit();
        }
        else if (e && e.hasOwnProperty('status') && e.status == 'PAGE_OPEN_EMPTY') {
            console.log('[PAGE_OPEN_EMPTY]');
            param.exit_case = 'PAGE_OPEN_EMPTY';
            return task_finish_post(page, param.task.results);
            // phantom.exit();
        }
        else {
            debug.log(script_name, '### CATCH! EXCEPTION!  ###                ');
            console.error(script_name, 'catch!', JSON.stringify(e));
            debug.log(script_name, 'phantom.exit();');
            phantom.exit();
        }

    }).delay(2000).done(function (send_data_result) {

        debug.log(script_name, colors.bgWhite.blue.bold('### step 7 - FINISHED  ###              '));
        //debug.log(script_name, 'page.plainText',page.plainText);
        debug.log(script_name, 'phantom.exit();');
        console.log('[FULL_FINISHED]');

        phantom.exit(0);

    });

}

function init() {

    var deferred = Q.defer();

    debug.log(script_name, 'init() start');
    page = webpage.create();

    if (param.task.disable_img_js) {
        //obj.settings.javascriptEnabled             = false;
       // obj.settings.loadImages = false;
    }

    page.open('about:blank', function (status) {
        debug.log(script_name, 'init() about:blank', status);
        debug.log(script_name, 'init() deferred.resolve(true);');
        deferred.resolve(true);
    });


    return deferred.promise;

}
